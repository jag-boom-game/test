function PlayerStateRoll() {
	// Movement
	hspd = lengthdir_x(speedRoll,direction);
	vspd = lengthdir_y(speedRoll,direction);

	moveDistanceRemaining = max(0, moveDistanceRemaining - speedRoll);
	var _collided = PlayerCollision();

	// Update sprite
	sprite_index = spriteRoll;
	var _totalFrames = sprite_get_number(sprite_index)/4;
	image_index = (CARDINAL_DIR * _totalFrames) + min(((1 - (moveDistanceRemaining / distanceRoll)) * _totalFrames), _totalFrames - 1);

	// Change state
	if (moveDistanceRemaining <=0) 
	{
		state = PlayerStateFree;
	}

	if (_collided)
	{
			audio_play_sound(sn_Bump,1,false);
		state = PlayerStateBonk
		moveDistanceRemaining = distanceBonk
		ScreenShake( 4,30)
	}


}
