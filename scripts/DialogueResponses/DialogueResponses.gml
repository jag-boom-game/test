/// @arg Response
function DialogueResponses(argument0) {
	switch(oPlayer.activate.name) {
		case "Shopkeeper Joe":
		switch(argument0)
		{
			case 0: break 
			case 1: NewTextBox("You flip him the middle finger and walk away.", 0); break;
			case 2: NewTextBox("Are you interested in an epic quest?", 0, ["3: Yes!" , "4:No"]); break;
			case 3: NewTextBox("Great! You're in for a treat! Cue the cutscene! \n I said, CUE THE CUTSCENE, GEOFFREY!", 0); break;
			case 4: NewTextBox("That's a shame because it's a really good quest!",0); break;
			default: break;
		}
		break;
		
		case "Pond Man":
		switch(argument0)
		{
			case 0: break 
			case 1: NewTextBox("Oddly strong feelings towards a color, \n but alright.", 0); break;
			case 2: NewTextBox("Hey thanks! \n It's kind of a magic pond!", 0, ["3: What do you mean?" , "4:Get bent, Pond Man!"]); break;
			case 3: NewTextBox("It's said that if you throw some \n rupees in, something good will happen!", 0, ["5: That's wildly unsubtle, but thanks!"]); break;
			case 4: NewTextBox("You too, buddy! \n *He flips you off.*",0); break;
			case 5: NewTextBox("I'm doing my part!",0); break;
			default: break;
		}
		break;
		
		case "Aaron":
		switch(argument0)
		{
			case 0: break 
			case 1: NewTextBox("Oddly strong feelings towards a color, \n but alright.", 0); break;
			case 2: NewTextBox("Hey thanks! \n It's kind of a magic pond!", 0, ["3: What do you mean?" , "4:Get bent, Pond Man!"]); break;
			case 3: NewTextBox("It's said that if you throw some \n rupees in, something good will happen!", 0, ["5: That's wildly unsubtle, but thanks!"]); break;
			case 4: NewTextBox("You too, buddy! \n *He flips you off.*",0); break;
			case 5: NewTextBox("I'm doing my part!",0); break;
			default: break;
		}
		break;
		
		case "Steve":
		switch(argument0)
		{
			case 0: break 
			case 1: NewTextBox("Hey! Fuck you too! And get off my lawn!", 0); break;
			case 2: NewTextBox("Well, good afternoon, my polite young man! \n Care to shoot the breeze?", 0, ["3: Yes!" , "10:No"]); break;
			case 3: NewTextBox("Terrific! Let me tell you about the history of our people! \n You see, we weren't always cats! Well, sort of...", 0, ["4: Continue listening?" , "10: Fake being asleep?"]); break;
			case 4: NewTextBox("The thing is cats aren't common in all planes. \n In fact, hairless cat-people, 'humans' actually rule many.",0, ["5:Continue listening?" , "10: Snore loudly?"]); break;
			case 5: NewTextBox("My father was a cat like any other. \n His name was Lion-O.",0, ["6:Continue listening?" , "10: Literally pass out?"]); break;
			case 6: NewTextBox("He was the leader of a team called the Lightning Felines. \n ...or it was something like that.",0, ["7:Continue listening?" , "10: Snore loudly?"]); break;
			case 7: NewTextBox("Anyway, can I get you a glass of milk? Or, if you'd like to \n engage The Device, I have some cat nip in the back...",0, ["8:Continue listening?" , "10: Fake a diabetic coma?"]); break;
			case 8: NewTextBox("Wouldn't want your employer to get angry. I suppose \n I should let you go. It's a beatiful day after all.",0, ["9:Continue listening?" , "10: Completely fucking zone out?"]); break;
			case 9: NewTextBox("You know, you're such a polite young man, for listening. \n I hope you've learned something here.",0, ["11: Thank you, sir! Have a nice day!"]);
				if (oGame.journal_entries_locked[? 0] == false) {
					ds_map_set(oGame.journal_entries_locked,0,true);
					NewJournal();
					}
			break;
			case 10: NewTextBox("Perhaps another time, then...",0); break;
			case 11: NewTextBox("You too, my friend!",0); break;
			default: break;
		}
		break;

		default: NewTextBox("This character isn't feeling particularly chatty.");
	}

}
