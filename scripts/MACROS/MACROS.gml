function MACROS() {
	#macro FRAME_RATE 60
	#macro TILE_SIZE 8
	#macro CARDINAL_DIR round(direction/90)
	#macro ROOM_START rNewVillage

	#macro RESOLUTION_W 320
	#macro RESOLUTION_H 180

	#macro TRANSITION_SPEED 0.02
	#macro IN 1
	#macro OUT 0

	enum ENEMYSTATE
	{
		IDLE,
		WANDER,
		CHASE,
		ATTACK,
		HURT,
		DIE,
		WAIT
	}
}
