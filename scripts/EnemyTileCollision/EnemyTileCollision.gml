
function EnemyTileCollision(){
	// Horizontal Tiles
	var _collision = false;
	if (tilemap_get_at_pixel(collisionmap, x + hspd, y))
	{
		x -= x mod TILE_SIZE;
		if (sign(hspd) == 1) {x += TILE_SIZE - 1;}
		hspd = 0;
		_collision = true;
	}
	x += hspd;
	
	// Vertical Tiles
	
	if (tilemap_get_at_pixel(collisionmap, x, y + vspd))
	{
		y -= y mod TILE_SIZE;
		if (sign(vspd) == 1){ y += TILE_SIZE - 1;}
		vspd = 0;
		_collision = true;
	}
	y += vspd;
	
	return _collision;
}