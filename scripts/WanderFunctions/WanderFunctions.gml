function BirdWander()
{
	sprite_index = sprMove
	
	//At Destination
	if (( x == xTo) && (y == yTo)) || (timePassed > enemyWanderDistance / enemySpeed)
	{
		hspd = 0
		vspd = 0
		//End our move animation
		if (image_index < 1)
		{
			sprite_index = sprIdle
			image_speed = 0.0
			image_index = 0
		}
		
		//Set new target destination
		if (++wait >= waitDuration)
		{
			wait = 0
			timePassed = 0
			dir = point_direction(x,y,xstart,ystart) + irandom_range(-45,45)
			xTo = x + lengthdir_x(enemyWanderDistance, dir)
			yTo = y + lengthdir_y(enemyWanderDistance, dir)
		}
	}
	else //Otherwise move towards new destination
	{
		timePassed++
		image_speed = 1.0
		var _distanceToGo = point_distance(x,y,xTo,yTo)
		var _speedThisFrame = enemySpeed
		if(_distanceToGo < enemySpeed) _speedThisFrame = _distanceToGo
		dir = point_direction(x,y,xTo,yTo)
		hspd = lengthdir_x(_speedThisFrame, dir)
		vspd = lengthdir_y(_speedThisFrame, dir)
		if (hspd != 0) image_xscale = sign(hspd);
		image_xscale *= original_scale;
		
		//Collide and move
		EnemyTileCollision()
		
	}
}

function ButterflyWander()
{
	sprite_index = sprMove
	
	//Set new target destination
	if (++wait >= waitDuration)
	{
		wait = 0
		timePassed = 0
		dir = point_direction(x,y,xstart,ystart) + irandom_range(-45,45)
		xTo = x + lengthdir_x(enemyWanderDistance, dir)
		yTo = y + lengthdir_y(enemyWanderDistance, dir)
	}
	else //Otherwise move towards new destination
	{
		timePassed++
		image_speed = 1.0
		var _distanceToGo = point_distance(x,y,xTo,yTo)
		var _speedThisFrame = enemySpeed
		if(_distanceToGo < enemySpeed) _speedThisFrame = _distanceToGo
		dir = point_direction(x,y,xTo,yTo)
		hspd = lengthdir_x(_speedThisFrame, dir)
		vspd = lengthdir_y(_speedThisFrame, dir)
		if (hspd != 0) image_xscale = sign(hspd);
		image_xscale *= original_scale;
		
		//Collide and move
		EnemyTileCollision()	
	}
}