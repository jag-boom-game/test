function PlayerStateFree() {
	// Movement
		hspd = lengthdir_x(inputmagnitude * speedwalk, inputdirection)
		vspd = lengthdir_y(inputmagnitude * speedwalk, inputdirection)

		PlayerCollision();

	// Update Sprite Index
		var _oldsprite = sprite_index;
		if (inputmagnitude != 0)
			{
				direction = inputdirection
				sprite_index = spriteRun;
			} else sprite_index = spriteIdle;
		if (_oldsprite != sprite_index) localFrame = 0;

	// Update image index
		PlayerAnimateSprite();

	// Magic Key Logic 
	if (global.keyMagic)
		{
			state = PlayerStateMagic;
			stateMagic = magic;
		}

	//  Attack Key Logic
		if (global.keyAttack)
			{
				state = PlayerStateAttack
				stateAttack = AttackSlash
			}
	
	// Activate Key Logic
	if (global.keyActivate) 
		{
			//1. Check for an entity to activate
			//2. If there is nothing, or there is something but it has no script
				//2a. If we are carrying something, throw it!
				//2b. Otherwise, roll!
			//3. Otherwise, there is something and it has a script! Activate!
			//4. If the thing we activate is an NPC, make it face towards us.
	
			var _activateX = x + lengthdir_x(10,direction)
			var _activateY = y + lengthdir_y(10,direction)
			var _activateSize = 4
			var _activateList = ds_list_create()
			activate = noone
			var _entitiesFound = collision_rectangle_list(
			_activateX - _activateSize,
			_activateY - _activateSize,
			_activateX + _activateSize,
			_activateY + _activateSize,
			pEntity,
			false,
			true,
			_activateList,
			true
			);
			
			// If the first instance we find is either our lifted entity or it has no script:try the next one
			while (_entitiesFound > 0)
			{
				var _check = _activateList[| -- _entitiesFound];
				if (_check != global.iLifted) && (_check.entityActivateScript != -1)
				{
					activate = _check
					break;
				}
			}
			
			ds_list_destroy(_activateList)
			if (global.iLifted != noone) {activate = noone;} //This is the code I added. 
			if (activate == noone)
			{
				// Throw something if held, otherwise roll
				if (global.iLifted != noone)
				{
					PlayerThrow()
				}
				else
				{
					state = PlayerStateRoll;
					moveDistanceRemaining = distanceRoll;
				}
			}
			else
			{
					// Activate the Entity	
					ScriptExecuteArray(activate.entityActivateScript,activate.entityActivateArgs)
				
					// Make an NPC face the Player
					if (activate.entityNPC)
					{
						with(activate)
						{
							direction = point_direction(x,y,other.x,other.y)
							image_index = CARDINAL_DIR
						}
					}
			}	
		}
}
