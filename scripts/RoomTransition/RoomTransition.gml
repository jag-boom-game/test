/// @desc RoomTransition(type, targetroom)
/// @arg Type
/// @arg TargetRoom

function RoomTransition (argument0,argument1) {
	if instance_exists(oPlayer) {

		if (!instance_exists(oTransition))
		{
			with (instance_create_depth(0,0,-9999,oTransition))
			{
				type = argument[0];
				target = argument[1];
			}
		} else show_debug_message("Trying to transition with transition is already happening!");
	} else {/*
		instance_create_depth(global.targetX,global.targetY,depth,oPlayer);
		if instance_exists(oPlayer) {
		if (!instance_exists(oTransition))
		{
			with (instance_create_depth(0,0,-9999,oTransition))
			{
				type = argument[0];
				target = argument[1];
			}
		} else show_debug_message("Trying to transition with transition is already happening!");
		}
*/	} 
}