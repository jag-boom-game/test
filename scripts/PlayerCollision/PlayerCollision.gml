	function PlayerCollision() {
		if (instance_exists(oPlayer)) {
			var _collision = false;
			var _entityList = ds_list_create();

			// Horizontal Tiles
			if (tilemap_get_at_pixel(oPlayer.collisionmap, x + oPlayer.hspd, y))
			{
				x -= x mod TILE_SIZE
				if (sign(hspd) == 1) x += TILE_SIZE - 1;
				hspd = 0;
				_collision = true;
			}

			// Horizontal Entities
			var _entityCount = instance_position_list(x + hspd, y, pEntity, _entityList, false)
			var _snapX;
			while (_entityCount > 0)
			{
				var _entityCheck = _entityList[| 0]; 
				if (_entityCheck.entityCollision == true)
				{
					// Move as close as we can
					if (sign(hspd) == -1) _snapX = _entityCheck.bbox_right+1
					else _snapX = _entityCheck.bbox_left-1
					x = _snapX
					hspd = 0
					_collision = true
					_entityCount = 0
				}
				ds_list_delete(_entityList,0)
				_entityCount--;
			}

			// Horizontal Move Comit
			x += hspd;

			// Clear List Between Axis
			ds_list_clear(_entityList)

			// Vertical Tiles
			if (tilemap_get_at_pixel(collisionmap, x, y + vspd))
			{
				y -= y mod TILE_SIZE
				if (sign(vspd) == 1) y += TILE_SIZE - 1;
				vspd = 0;
				_collision = true;
	
			}

			// Vertical Entities
			var _entityCount = instance_position_list(x, y + vspd, pEntity, _entityList, false)
			var _snapY;
			while (_entityCount > 0)
			{
				var _entityCheck = _entityList[| 0]; 
				if (_entityCheck.entityCollision == true)
				{
					// Move as close as we can
					if (sign(vspd) == -1) _snapY = _entityCheck.bbox_bottom+1
					else _snapY = _entityCheck.bbox_top-1
					y = _snapY
					vspd = 0
					_collision = true
					_entityCount = 0
				}
				ds_list_delete(_entityList,0)
				_entityCount--;
			}
			// Vertical Move Comit
			y += vspd;
			ds_list_destroy(_entityList)
			return _collision;
		}
	}
