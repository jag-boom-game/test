
function SaveGame(){
	debug = "Game " + string(menu_option) + " saved!"
	var _root_list = ds_list_create();
	
	var _map = ds_map_create();
	ds_list_add(_root_list, _map);
	ds_list_mark_as_map(_root_list,ds_list_size(_root_list)-1);
	
	var _obj = object_get_name(object_index);
	ds_map_add(_map, "obj", _obj);
	ds_map_add(_map, "y", oPlayer.y);
	ds_map_add(_map, "x", oPlayer.x);
	ds_map_add(_map, "image_index", oPlayer.image_index);
	ds_map_add(_map, "image_blend", oPlayer.image_blend);
	ds_map_add(_map, "room", room);
	
	var _wrapper = ds_map_create();
	
	ds_map_add_list(_wrapper, "ROOT", _root_list);

	var _string = json_encode(_wrapper);
	SaveStringToFile("savedgame" + string(menu_option) + ".sav", _string);

	ds_map_destroy(_wrapper);
	ds_list_destroy(_root_list);
	show_debug_message(debug);
}