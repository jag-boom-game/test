{
  "compression": 0,
  "volume": 0.15,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sn_EnemyHit.wav",
  "duration": 0.658424,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "sn_EnemyHit",
  "tags": [],
  "resourceType": "GMSound",
}