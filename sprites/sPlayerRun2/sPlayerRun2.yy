{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 14,
  "bbox_top": 14,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 17,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4260d190-bd64-4050-93e6-125226468c28","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4260d190-bd64-4050-93e6-125226468c28","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"4260d190-bd64-4050-93e6-125226468c28","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e9ad5b4-e0ed-4776-852b-efd78dc79771","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e9ad5b4-e0ed-4776-852b-efd78dc79771","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"0e9ad5b4-e0ed-4776-852b-efd78dc79771","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee5ad79e-afcc-403e-8363-fd3ffc791996","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee5ad79e-afcc-403e-8363-fd3ffc791996","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"ee5ad79e-afcc-403e-8363-fd3ffc791996","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"167d8e52-f781-4609-a31b-76add6a3470c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"167d8e52-f781-4609-a31b-76add6a3470c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"167d8e52-f781-4609-a31b-76add6a3470c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8aead8d-feae-4690-acd5-e84c3c8f8c11","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8aead8d-feae-4690-acd5-e84c3c8f8c11","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"a8aead8d-feae-4690-acd5-e84c3c8f8c11","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"47bfb2a7-f678-48e0-bd1c-8d1d2890e14c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"47bfb2a7-f678-48e0-bd1c-8d1d2890e14c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"47bfb2a7-f678-48e0-bd1c-8d1d2890e14c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c959fe9d-8744-448b-88ac-47f301b7e247","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c959fe9d-8744-448b-88ac-47f301b7e247","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"c959fe9d-8744-448b-88ac-47f301b7e247","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"252445ed-8872-4ff6-8a82-9e420fee3691","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"252445ed-8872-4ff6-8a82-9e420fee3691","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"252445ed-8872-4ff6-8a82-9e420fee3691","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ace62a60-de32-40ea-b746-f1aeae3b3ed0","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ace62a60-de32-40ea-b746-f1aeae3b3ed0","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"ace62a60-de32-40ea-b746-f1aeae3b3ed0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d4b33278-86c0-4815-9b31-ba88b2cf7d71","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d4b33278-86c0-4815-9b31-ba88b2cf7d71","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"d4b33278-86c0-4815-9b31-ba88b2cf7d71","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a98b3929-4a03-4aed-aea3-75c5c6c97f35","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a98b3929-4a03-4aed-aea3-75c5c6c97f35","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"a98b3929-4a03-4aed-aea3-75c5c6c97f35","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"903cbc11-1c7c-4f3a-aa6b-c63046b93af1","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"903cbc11-1c7c-4f3a-aa6b-c63046b93af1","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"903cbc11-1c7c-4f3a-aa6b-c63046b93af1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a3006828-ca6c-41a3-b3ec-858dd10d3fb5","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a3006828-ca6c-41a3-b3ec-858dd10d3fb5","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"a3006828-ca6c-41a3-b3ec-858dd10d3fb5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f58efe06-3961-4e8c-974e-3b225d1a8e47","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f58efe06-3961-4e8c-974e-3b225d1a8e47","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"f58efe06-3961-4e8c-974e-3b225d1a8e47","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"58bbaf7f-d2b6-4f3c-8ebd-20743bb980ff","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"58bbaf7f-d2b6-4f3c-8ebd-20743bb980ff","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"58bbaf7f-d2b6-4f3c-8ebd-20743bb980ff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00fd6dd9-33ed-4464-818c-b6fc260e830c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00fd6dd9-33ed-4464-818c-b6fc260e830c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"00fd6dd9-33ed-4464-818c-b6fc260e830c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"81a46a56-cb18-4666-9e4a-d698404eac8e","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"81a46a56-cb18-4666-9e4a-d698404eac8e","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"81a46a56-cb18-4666-9e4a-d698404eac8e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9115c7d7-3e01-418e-8c80-4b1f81525190","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9115c7d7-3e01-418e-8c80-4b1f81525190","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"9115c7d7-3e01-418e-8c80-4b1f81525190","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"11bf137b-3488-4010-ba23-726def624bd7","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"11bf137b-3488-4010-ba23-726def624bd7","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"11bf137b-3488-4010-ba23-726def624bd7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"201929e9-4c94-47f1-b24b-0b729138c2d4","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"201929e9-4c94-47f1-b24b-0b729138c2d4","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"201929e9-4c94-47f1-b24b-0b729138c2d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"82e163c2-e6f7-4f7f-9a21-aab2bcaf4eff","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"82e163c2-e6f7-4f7f-9a21-aab2bcaf4eff","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"82e163c2-e6f7-4f7f-9a21-aab2bcaf4eff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c5bf8ed4-9ca5-4412-948d-ce9962461976","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c5bf8ed4-9ca5-4412-948d-ce9962461976","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"c5bf8ed4-9ca5-4412-948d-ce9962461976","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af6cd249-983b-4c02-afdc-7988e3255adb","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af6cd249-983b-4c02-afdc-7988e3255adb","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"af6cd249-983b-4c02-afdc-7988e3255adb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b6e62d74-a86f-4abd-9fd4-6c839d706039","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b6e62d74-a86f-4abd-9fd4-6c839d706039","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"b6e62d74-a86f-4abd-9fd4-6c839d706039","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"afac52c8-4917-40e5-8e4e-f1481a7149cf","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"afac52c8-4917-40e5-8e4e-f1481a7149cf","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"afac52c8-4917-40e5-8e4e-f1481a7149cf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f76ab3d5-0106-4029-b7f1-6d45cac31b31","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f76ab3d5-0106-4029-b7f1-6d45cac31b31","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"f76ab3d5-0106-4029-b7f1-6d45cac31b31","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f1544f8c-0d4d-4696-870b-9c7b3c2eb001","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f1544f8c-0d4d-4696-870b-9c7b3c2eb001","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"f1544f8c-0d4d-4696-870b-9c7b3c2eb001","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8deb66cc-b9c9-469f-868e-e337c9cc072d","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8deb66cc-b9c9-469f-868e-e337c9cc072d","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"8deb66cc-b9c9-469f-868e-e337c9cc072d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bea03780-e5c1-4db5-b35a-f002f6019bf6","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bea03780-e5c1-4db5-b35a-f002f6019bf6","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"bea03780-e5c1-4db5-b35a-f002f6019bf6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb6d4ee9-db2b-45f8-8f87-9dbe31f724c0","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb6d4ee9-db2b-45f8-8f87-9dbe31f724c0","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"eb6d4ee9-db2b-45f8-8f87-9dbe31f724c0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"041ba438-a10c-42fa-b908-20245b7a99db","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"041ba438-a10c-42fa-b908-20245b7a99db","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"041ba438-a10c-42fa-b908-20245b7a99db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7565c4b7-3afe-4db3-9ad1-2db8b025aa8b","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7565c4b7-3afe-4db3-9ad1-2db8b025aa8b","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"LayerId":{"name":"a93fc47b-3695-4e30-a669-2db3284c9954","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","name":"7565c4b7-3afe-4db3-9ad1-2db8b025aa8b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e48723c3-9f33-4616-9a87-8a45c0efd24b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4260d190-bd64-4050-93e6-125226468c28","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2466616f-fec5-4a25-9c45-3c81af8a88c2","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e9ad5b4-e0ed-4776-852b-efd78dc79771","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"208f9ed0-f74a-43ed-a666-aadbfed862a0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee5ad79e-afcc-403e-8363-fd3ffc791996","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9832621f-eb7a-4044-9ddd-6fa7382fa03c","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"167d8e52-f781-4609-a31b-76add6a3470c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"70dee584-db1e-4682-a3d6-5b0bbab1ac6a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8aead8d-feae-4690-acd5-e84c3c8f8c11","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8ef12972-18fa-4684-af2f-940ce0db4aa4","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"47bfb2a7-f678-48e0-bd1c-8d1d2890e14c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f231a159-fa5e-4d9b-9c49-8b91c4cd7793","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c959fe9d-8744-448b-88ac-47f301b7e247","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"324dc54a-5cb3-4fde-97fe-f44942e5ef8f","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"252445ed-8872-4ff6-8a82-9e420fee3691","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd2e84d4-7312-4983-913b-b65e1fa3929e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ace62a60-de32-40ea-b746-f1aeae3b3ed0","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"14e65b35-7bf8-495e-98ca-28a7c17b625d","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4b33278-86c0-4815-9b31-ba88b2cf7d71","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6766b230-dde9-4e4e-9e4d-17be8e31deec","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a98b3929-4a03-4aed-aea3-75c5c6c97f35","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8569c99-29f7-4426-bdb3-12728d7a532f","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"903cbc11-1c7c-4f3a-aa6b-c63046b93af1","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a29599fa-002c-40b5-bb94-e34b46ad6533","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a3006828-ca6c-41a3-b3ec-858dd10d3fb5","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b4c476a-1e5c-432e-a533-276d8bf0ff99","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f58efe06-3961-4e8c-974e-3b225d1a8e47","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c194a612-8ca2-486d-ae1b-54f139ed333a","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"58bbaf7f-d2b6-4f3c-8ebd-20743bb980ff","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"202a3252-17c4-412e-831a-5b0f152fd5f2","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00fd6dd9-33ed-4464-818c-b6fc260e830c","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c192afd8-261c-40d6-b906-9d27310dadd6","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81a46a56-cb18-4666-9e4a-d698404eac8e","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"91e88e45-412a-4eb1-8c23-2940f7183dc0","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9115c7d7-3e01-418e-8c80-4b1f81525190","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"53e61789-6995-44b0-9d6f-0abee845bcb9","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"11bf137b-3488-4010-ba23-726def624bd7","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1797988b-79da-46c5-b959-e345e1f25da1","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"201929e9-4c94-47f1-b24b-0b729138c2d4","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1587622e-d403-4dc4-9258-46b2ca802b1e","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"82e163c2-e6f7-4f7f-9a21-aab2bcaf4eff","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e92dd730-c01f-4cb9-8c3a-bca4d32c7c03","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c5bf8ed4-9ca5-4412-948d-ce9962461976","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82ada6c3-4585-4d0b-8fc3-2d3a6e436966","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af6cd249-983b-4c02-afdc-7988e3255adb","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4cbd08f2-c8ec-4907-a9be-e448e416c924","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b6e62d74-a86f-4abd-9fd4-6c839d706039","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4bc98d2b-702e-44ac-8008-0745e58d8dbd","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"afac52c8-4917-40e5-8e4e-f1481a7149cf","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ba0f305c-f497-4932-bb1d-e15aeeebde37","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f76ab3d5-0106-4029-b7f1-6d45cac31b31","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5cd9ba52-509c-43a6-8826-face4e6c1419","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f1544f8c-0d4d-4696-870b-9c7b3c2eb001","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c27c7e42-df9b-42ba-bdf8-6bc22fc29874","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8deb66cc-b9c9-469f-868e-e337c9cc072d","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c586767-cb14-4bd6-b274-69d4c72c6a32","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bea03780-e5c1-4db5-b35a-f002f6019bf6","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5535a54f-b0ba-4d2c-b9df-4bb441cdeb03","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb6d4ee9-db2b-45f8-8f87-9dbe31f724c0","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e55eeac-bdd6-48ef-b6a1-c01d25e2d209","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"041ba438-a10c-42fa-b908-20245b7a99db","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ef5d0e4-9f8b-4681-ad76-0679ddbcbc15","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7565c4b7-3afe-4db3-9ad1-2db8b025aa8b","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sPlayerRun2","path":"sprites/sPlayerRun2/sPlayerRun2.yy",},
    "resourceVersion": "1.3",
    "name": "sPlayerRun2",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a93fc47b-3695-4e30-a669-2db3284c9954","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player Sprites",
    "path": "folders/Sprites/Player Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sPlayerRun2",
  "tags": [],
  "resourceType": "GMSprite",
}