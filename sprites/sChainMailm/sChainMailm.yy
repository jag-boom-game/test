{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 17,
  "bbox_right": 46,
  "bbox_top": 32,
  "bbox_bottom": 55,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"cce9cb9a-fbd5-4720-98fc-2afd012e4dc9","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cce9cb9a-fbd5-4720-98fc-2afd012e4dc9","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"cce9cb9a-fbd5-4720-98fc-2afd012e4dc9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"950dca39-e498-443d-903c-63971bad6cd7","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"950dca39-e498-443d-903c-63971bad6cd7","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"950dca39-e498-443d-903c-63971bad6cd7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b666f074-3efe-4acd-a5f0-8d3b163d33af","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b666f074-3efe-4acd-a5f0-8d3b163d33af","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"b666f074-3efe-4acd-a5f0-8d3b163d33af","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d00c5afb-e222-4ef1-841f-7fb3f8e2eab0","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d00c5afb-e222-4ef1-841f-7fb3f8e2eab0","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"d00c5afb-e222-4ef1-841f-7fb3f8e2eab0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"04604cbf-3f86-43e4-807b-4e3982f273da","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04604cbf-3f86-43e4-807b-4e3982f273da","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"04604cbf-3f86-43e4-807b-4e3982f273da","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"821bfea2-1346-425c-930f-be509188005a","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"821bfea2-1346-425c-930f-be509188005a","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"821bfea2-1346-425c-930f-be509188005a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"120251ee-6616-4bda-ba16-bdbf941072fa","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"120251ee-6616-4bda-ba16-bdbf941072fa","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"120251ee-6616-4bda-ba16-bdbf941072fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8d780a7e-f17a-4bf8-81bb-bbd5176d7737","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8d780a7e-f17a-4bf8-81bb-bbd5176d7737","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"8d780a7e-f17a-4bf8-81bb-bbd5176d7737","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"762e31fa-a566-4380-accd-74e5c22a0799","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"762e31fa-a566-4380-accd-74e5c22a0799","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"762e31fa-a566-4380-accd-74e5c22a0799","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"591d0a67-a4ab-4d81-83d4-c2952d0f18cc","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"591d0a67-a4ab-4d81-83d4-c2952d0f18cc","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"591d0a67-a4ab-4d81-83d4-c2952d0f18cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c0c3a6e-64da-4a2a-8014-bbf22b1e0b5c","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c0c3a6e-64da-4a2a-8014-bbf22b1e0b5c","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"3c0c3a6e-64da-4a2a-8014-bbf22b1e0b5c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e8e83e2-b1a0-4e45-85aa-2e5c1af0beb3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e8e83e2-b1a0-4e45-85aa-2e5c1af0beb3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"0e8e83e2-b1a0-4e45-85aa-2e5c1af0beb3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"295775d9-5f7f-4f63-b528-3bf3cb73b0e2","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"295775d9-5f7f-4f63-b528-3bf3cb73b0e2","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"295775d9-5f7f-4f63-b528-3bf3cb73b0e2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"11299ddd-e248-4640-9906-bccc689b0ea3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"11299ddd-e248-4640-9906-bccc689b0ea3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"11299ddd-e248-4640-9906-bccc689b0ea3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a4e0a59d-ed71-45c2-8b4a-3e140c8307d7","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a4e0a59d-ed71-45c2-8b4a-3e140c8307d7","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"a4e0a59d-ed71-45c2-8b4a-3e140c8307d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"19c1ce06-400d-4d1f-882c-ddedeb574b08","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"19c1ce06-400d-4d1f-882c-ddedeb574b08","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"19c1ce06-400d-4d1f-882c-ddedeb574b08","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eaeec408-30ca-46db-bae4-e53bf25a3627","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eaeec408-30ca-46db-bae4-e53bf25a3627","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"eaeec408-30ca-46db-bae4-e53bf25a3627","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6367033a-fdd7-45d2-8899-c8a9a3f77d5a","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6367033a-fdd7-45d2-8899-c8a9a3f77d5a","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"6367033a-fdd7-45d2-8899-c8a9a3f77d5a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"09c94a2a-75af-4733-8464-e3d52c1cd51b","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"09c94a2a-75af-4733-8464-e3d52c1cd51b","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"09c94a2a-75af-4733-8464-e3d52c1cd51b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"195255a1-dc6b-46c1-8962-a900c654a268","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"195255a1-dc6b-46c1-8962-a900c654a268","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"195255a1-dc6b-46c1-8962-a900c654a268","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a1c07479-a9bd-4013-be32-490d430d18b3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a1c07479-a9bd-4013-be32-490d430d18b3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"a1c07479-a9bd-4013-be32-490d430d18b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d2eccd99-9b1c-48ed-a319-d42df3fab80e","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d2eccd99-9b1c-48ed-a319-d42df3fab80e","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"d2eccd99-9b1c-48ed-a319-d42df3fab80e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c8cb560-9a7e-4f02-9de4-aed3d52a86d1","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c8cb560-9a7e-4f02-9de4-aed3d52a86d1","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"5c8cb560-9a7e-4f02-9de4-aed3d52a86d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"58c305b2-efce-4893-9bdf-d11d77db1070","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"58c305b2-efce-4893-9bdf-d11d77db1070","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"58c305b2-efce-4893-9bdf-d11d77db1070","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"679aff3e-abfa-4ada-9732-9f2c19c813c3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"679aff3e-abfa-4ada-9732-9f2c19c813c3","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"679aff3e-abfa-4ada-9732-9f2c19c813c3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c69ab868-b7c3-42dc-831f-5215056ac37a","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c69ab868-b7c3-42dc-831f-5215056ac37a","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"c69ab868-b7c3-42dc-831f-5215056ac37a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1c553097-b076-4cf7-bdb2-b1b4eae15661","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c553097-b076-4cf7-bdb2-b1b4eae15661","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"1c553097-b076-4cf7-bdb2-b1b4eae15661","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6159a597-7042-44a5-a8e4-a750fee60146","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6159a597-7042-44a5-a8e4-a750fee60146","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"6159a597-7042-44a5-a8e4-a750fee60146","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c7928cdb-0eb2-4021-8cd1-c01a141fbabc","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c7928cdb-0eb2-4021-8cd1-c01a141fbabc","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"c7928cdb-0eb2-4021-8cd1-c01a141fbabc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"35d526ef-66ab-40c4-a19b-c8eb5896da0e","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"35d526ef-66ab-40c4-a19b-c8eb5896da0e","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"35d526ef-66ab-40c4-a19b-c8eb5896da0e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06441152-bb1b-4fa5-9a97-f36d3ce5fb47","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06441152-bb1b-4fa5-9a97-f36d3ce5fb47","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"06441152-bb1b-4fa5-9a97-f36d3ce5fb47","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53b85341-2a86-4ce1-96f3-e7cb87a7bc43","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53b85341-2a86-4ce1-96f3-e7cb87a7bc43","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"53b85341-2a86-4ce1-96f3-e7cb87a7bc43","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"72ca34ba-0213-4b4f-9d53-2e3f99caf03e","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72ca34ba-0213-4b4f-9d53-2e3f99caf03e","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"72ca34ba-0213-4b4f-9d53-2e3f99caf03e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"68e1f0d5-c34c-4e0d-b34a-1fb9ecbe3d04","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"68e1f0d5-c34c-4e0d-b34a-1fb9ecbe3d04","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"68e1f0d5-c34c-4e0d-b34a-1fb9ecbe3d04","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8e076049-ac7e-4518-ac51-7d1269190e20","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8e076049-ac7e-4518-ac51-7d1269190e20","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"8e076049-ac7e-4518-ac51-7d1269190e20","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d273fa71-5268-4e82-ad0c-34b9ca1ce529","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d273fa71-5268-4e82-ad0c-34b9ca1ce529","path":"sprites/sChainMailm/sChainMailm.yy",},"LayerId":{"name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","name":"d273fa71-5268-4e82-ad0c-34b9ca1ce529","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 36.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4dafd14f-dcd5-47d2-b262-02fcff57a2d5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cce9cb9a-fbd5-4720-98fc-2afd012e4dc9","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbdb7499-3553-4e8f-990a-40a42e7dab8a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"950dca39-e498-443d-903c-63971bad6cd7","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"742c0507-dac4-4ac0-abd1-7552bf5e31e8","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b666f074-3efe-4acd-a5f0-8d3b163d33af","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bdef6de7-638c-43ac-82d3-17d071e0c42e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d00c5afb-e222-4ef1-841f-7fb3f8e2eab0","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"43057ebd-500f-4ff0-8982-a8b7f36fd03a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04604cbf-3f86-43e4-807b-4e3982f273da","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e908c56b-5380-4ed7-905a-7a8aa40d5632","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"821bfea2-1346-425c-930f-be509188005a","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6ccb01bb-4547-4532-8f39-770ab312ce1f","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"120251ee-6616-4bda-ba16-bdbf941072fa","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0d90b02-716d-42cc-883e-63c10c823076","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8d780a7e-f17a-4bf8-81bb-bbd5176d7737","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc291858-f668-4660-bcf5-faa2614c276d","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"762e31fa-a566-4380-accd-74e5c22a0799","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1f87fe9b-7bfb-4a4c-a7ae-03948400be17","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"591d0a67-a4ab-4d81-83d4-c2952d0f18cc","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0af72b07-2613-45aa-b56b-41a1a1babeab","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c0c3a6e-64da-4a2a-8014-bbf22b1e0b5c","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e960dcba-edef-4c98-a2a0-803a647c8d46","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e8e83e2-b1a0-4e45-85aa-2e5c1af0beb3","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c67da4e5-f295-4973-8a59-34afcdc15ec1","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"295775d9-5f7f-4f63-b528-3bf3cb73b0e2","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6d310344-e986-4553-8d1b-5f63d927e41d","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"11299ddd-e248-4640-9906-bccc689b0ea3","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d7d47ea-2020-4c12-b7ca-c3b218bb87e0","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a4e0a59d-ed71-45c2-8b4a-3e140c8307d7","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d5102fb9-7441-48ac-8223-a9d81a26ec61","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"19c1ce06-400d-4d1f-882c-ddedeb574b08","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4483355e-d2bd-4b2b-b963-074702bc7787","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eaeec408-30ca-46db-bae4-e53bf25a3627","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6055320-c17b-4c29-a5e9-761e778d0a91","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6367033a-fdd7-45d2-8899-c8a9a3f77d5a","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1aa4a141-cc7a-46ad-8154-d4276784e104","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"09c94a2a-75af-4733-8464-e3d52c1cd51b","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2a4a5d6e-b3a6-4213-8923-b578b1f340c1","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"195255a1-dc6b-46c1-8962-a900c654a268","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1ff91c2d-c323-4cbc-a811-690a6519f57d","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a1c07479-a9bd-4013-be32-490d430d18b3","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d943fe6d-4ce2-483e-aec9-63f94954cdbd","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d2eccd99-9b1c-48ed-a319-d42df3fab80e","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afbfadd8-6366-4066-b99a-86c056c96e6f","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c8cb560-9a7e-4f02-9de4-aed3d52a86d1","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1842bc80-e2e4-4e31-b8c2-40fc6754170b","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"58c305b2-efce-4893-9bdf-d11d77db1070","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8b936e31-325a-4be4-8304-8ba3b05cf610","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"679aff3e-abfa-4ada-9732-9f2c19c813c3","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c3336857-3a86-44f3-82c0-038d903eed5f","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c69ab868-b7c3-42dc-831f-5215056ac37a","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f628b1a-0869-41e4-8e19-0321225df475","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c553097-b076-4cf7-bdb2-b1b4eae15661","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"beddec4a-02c1-463b-9a7b-5da59bc1da46","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6159a597-7042-44a5-a8e4-a750fee60146","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f6e7e9b6-41d0-4a61-9249-8f85ea9b31c8","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c7928cdb-0eb2-4021-8cd1-c01a141fbabc","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a66defc4-55b4-447d-8770-27f5a44cb098","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"35d526ef-66ab-40c4-a19b-c8eb5896da0e","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"797173b7-b0ee-464a-91ff-54d6b1b497d7","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06441152-bb1b-4fa5-9a97-f36d3ce5fb47","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"07876371-cb68-4bb3-a174-8c3d194342f1","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53b85341-2a86-4ce1-96f3-e7cb87a7bc43","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a2a5b3ea-ee1d-44e9-b036-0f6a50c0384e","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72ca34ba-0213-4b4f-9d53-2e3f99caf03e","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"28e8b414-0908-4e3e-83c7-04ce7f3adac3","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"68e1f0d5-c34c-4e0d-b34a-1fb9ecbe3d04","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"807c037e-6f63-464a-b25f-84c5f97d8299","Key":34.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8e076049-ac7e-4518-ac51-7d1269190e20","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6f39b019-5857-452d-9a28-2fe0395ec9e1","Key":35.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d273fa71-5268-4e82-ad0c-34b9ca1ce529","path":"sprites/sChainMailm/sChainMailm.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sChainMailm","path":"sprites/sChainMailm/sChainMailm.yy",},
    "resourceVersion": "1.3",
    "name": "sChainMailm",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"057f268a-2189-4cf4-88c7-e22c46ac58ea","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Equipment",
    "path": "folders/Sprites/Player Sprites/Equipment.yy",
  },
  "resourceVersion": "1.0",
  "name": "sChainMailm",
  "tags": [],
  "resourceType": "GMSprite",
}