{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 18,
  "bbox_right": 45,
  "bbox_top": 44,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"204b9220-c41a-4185-aa5e-db956f3395c0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"204b9220-c41a-4185-aa5e-db956f3395c0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"204b9220-c41a-4185-aa5e-db956f3395c0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0b62b812-6e95-4f75-862c-426151f8c151","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0b62b812-6e95-4f75-862c-426151f8c151","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"0b62b812-6e95-4f75-862c-426151f8c151","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"81147ea3-8a61-4da3-a0e2-64a93333e88a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"81147ea3-8a61-4da3-a0e2-64a93333e88a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"81147ea3-8a61-4da3-a0e2-64a93333e88a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f8a610a-befc-4947-9898-d250e652d420","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f8a610a-befc-4947-9898-d250e652d420","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"3f8a610a-befc-4947-9898-d250e652d420","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a493e8e1-ca1c-49d9-901b-055635fdd255","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a493e8e1-ca1c-49d9-901b-055635fdd255","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"a493e8e1-ca1c-49d9-901b-055635fdd255","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41ad2bd6-e6eb-4aee-a7ee-88a048bb77bb","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41ad2bd6-e6eb-4aee-a7ee-88a048bb77bb","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"41ad2bd6-e6eb-4aee-a7ee-88a048bb77bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"90268914-dd91-4672-8320-76ad7f857986","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90268914-dd91-4672-8320-76ad7f857986","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"90268914-dd91-4672-8320-76ad7f857986","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"68f47f4f-f136-456c-ad47-7d1e904f1a55","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"68f47f4f-f136-456c-ad47-7d1e904f1a55","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"68f47f4f-f136-456c-ad47-7d1e904f1a55","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f429bdc8-08e8-4788-9e8f-a3544a711dd1","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f429bdc8-08e8-4788-9e8f-a3544a711dd1","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"f429bdc8-08e8-4788-9e8f-a3544a711dd1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"47ffb177-1754-48d8-b04e-a8b936a1f01a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"47ffb177-1754-48d8-b04e-a8b936a1f01a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"47ffb177-1754-48d8-b04e-a8b936a1f01a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc3849c0-1846-4e94-bc82-c8fd11c76499","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc3849c0-1846-4e94-bc82-c8fd11c76499","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"cc3849c0-1846-4e94-bc82-c8fd11c76499","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9509fc5-56f9-497c-80a0-1f4c85acc8d0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9509fc5-56f9-497c-80a0-1f4c85acc8d0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"d9509fc5-56f9-497c-80a0-1f4c85acc8d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"38b5c266-7214-4002-888d-10db8c69b75e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"38b5c266-7214-4002-888d-10db8c69b75e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"38b5c266-7214-4002-888d-10db8c69b75e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4c350884-559d-4cb0-acc2-b40722773a01","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4c350884-559d-4cb0-acc2-b40722773a01","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"4c350884-559d-4cb0-acc2-b40722773a01","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b407dcf5-c9b2-43b0-a9cf-16753d82be83","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b407dcf5-c9b2-43b0-a9cf-16753d82be83","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"b407dcf5-c9b2-43b0-a9cf-16753d82be83","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ac1a739-62fa-4f5a-b9a0-2a8c72d378cd","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ac1a739-62fa-4f5a-b9a0-2a8c72d378cd","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"8ac1a739-62fa-4f5a-b9a0-2a8c72d378cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a10b5412-945e-4588-87a6-f17067791034","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a10b5412-945e-4588-87a6-f17067791034","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"a10b5412-945e-4588-87a6-f17067791034","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2db0b620-11f6-489c-831d-de18360d8aef","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2db0b620-11f6-489c-831d-de18360d8aef","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"2db0b620-11f6-489c-831d-de18360d8aef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22bd4525-02b2-48db-82e6-006d638046d0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22bd4525-02b2-48db-82e6-006d638046d0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"22bd4525-02b2-48db-82e6-006d638046d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a34e99d6-12a4-4634-8972-9ccfc50a654d","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a34e99d6-12a4-4634-8972-9ccfc50a654d","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"a34e99d6-12a4-4634-8972-9ccfc50a654d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fca1331c-6321-4142-9d39-5bced097878e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fca1331c-6321-4142-9d39-5bced097878e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"fca1331c-6321-4142-9d39-5bced097878e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a7cae5b7-84e2-43a7-9020-dcfbc3d437a0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a7cae5b7-84e2-43a7-9020-dcfbc3d437a0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"a7cae5b7-84e2-43a7-9020-dcfbc3d437a0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af367f12-8b68-409c-9275-a0bd128b143c","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af367f12-8b68-409c-9275-a0bd128b143c","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"af367f12-8b68-409c-9275-a0bd128b143c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb2aaa36-b55e-49d8-be91-c9f39e540943","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb2aaa36-b55e-49d8-be91-c9f39e540943","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"eb2aaa36-b55e-49d8-be91-c9f39e540943","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ec775f37-5846-4cc5-a7be-bd28d1edda7f","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ec775f37-5846-4cc5-a7be-bd28d1edda7f","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"ec775f37-5846-4cc5-a7be-bd28d1edda7f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b80d2861-26cd-4960-a7c4-a56b5fa0d3e7","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b80d2861-26cd-4960-a7c4-a56b5fa0d3e7","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"b80d2861-26cd-4960-a7c4-a56b5fa0d3e7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"15418c0a-4169-4847-8ea8-70a1b4850db4","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"15418c0a-4169-4847-8ea8-70a1b4850db4","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"15418c0a-4169-4847-8ea8-70a1b4850db4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0f7e0902-5a4d-47ee-84fc-b72e8db0fe54","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0f7e0902-5a4d-47ee-84fc-b72e8db0fe54","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"0f7e0902-5a4d-47ee-84fc-b72e8db0fe54","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c5b2cd0e-e74c-46c2-b037-e38df2f3b42e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c5b2cd0e-e74c-46c2-b037-e38df2f3b42e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"c5b2cd0e-e74c-46c2-b037-e38df2f3b42e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"31ebc5f0-8207-4c6b-959f-a968bf98f8a5","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31ebc5f0-8207-4c6b-959f-a968bf98f8a5","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"31ebc5f0-8207-4c6b-959f-a968bf98f8a5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8b9eed93-a820-48dd-a357-829361feab4a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8b9eed93-a820-48dd-a357-829361feab4a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"8b9eed93-a820-48dd-a357-829361feab4a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc5ea591-ef9f-457b-9685-4d90c79d0ae8","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc5ea591-ef9f-457b-9685-4d90c79d0ae8","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"fc5ea591-ef9f-457b-9685-4d90c79d0ae8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d0103681-8a6f-4401-90a5-3816a555da4b","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d0103681-8a6f-4401-90a5-3816a555da4b","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"d0103681-8a6f-4401-90a5-3816a555da4b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dfa8f9d7-f988-4e1c-ad5e-3c15a4b9c502","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dfa8f9d7-f988-4e1c-ad5e-3c15a4b9c502","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"dfa8f9d7-f988-4e1c-ad5e-3c15a4b9c502","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f28d968-3c51-407b-958c-55c8cebcf03f","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f28d968-3c51-407b-958c-55c8cebcf03f","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"7f28d968-3c51-407b-958c-55c8cebcf03f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e2b676b-57a5-46cc-b0f2-e42ee58e9db2","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e2b676b-57a5-46cc-b0f2-e42ee58e9db2","path":"sprites/sGoldBoots/sGoldBoots.yy",},"LayerId":{"name":"3d04a456-7790-4370-9ed3-b2385cf8b403","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","name":"0e2b676b-57a5-46cc-b0f2-e42ee58e9db2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 36.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5d4a2c7b-118c-4f9c-938a-2ec2fae7dc1e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"204b9220-c41a-4185-aa5e-db956f3395c0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7ed2d48d-681f-4e64-b608-7901ba2e82ff","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b62b812-6e95-4f75-862c-426151f8c151","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb9d2bcb-44f9-4caf-b466-5babb26b8fd2","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81147ea3-8a61-4da3-a0e2-64a93333e88a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c0db6f13-6ec3-48ac-b67d-9882cd5a2488","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f8a610a-befc-4947-9898-d250e652d420","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"915ad60f-b163-4c6e-98c6-7df24cf31c66","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a493e8e1-ca1c-49d9-901b-055635fdd255","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4ee3477c-1a32-4171-92c2-c104277071ac","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41ad2bd6-e6eb-4aee-a7ee-88a048bb77bb","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8874280-e184-4bfc-82dd-c5d2a65a66bf","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90268914-dd91-4672-8320-76ad7f857986","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b0359342-7e7a-4995-9196-f59dab3367bc","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"68f47f4f-f136-456c-ad47-7d1e904f1a55","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed1ba4fa-6156-44f3-a7f3-7a7cd1dc7070","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f429bdc8-08e8-4788-9e8f-a3544a711dd1","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3801a716-9a12-4788-8415-07744d4f1ac7","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"47ffb177-1754-48d8-b04e-a8b936a1f01a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ec922ae6-99c2-4017-8fc0-953c9d3756d2","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc3849c0-1846-4e94-bc82-c8fd11c76499","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d5280fb2-ab35-44a2-97e9-680f84560fde","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9509fc5-56f9-497c-80a0-1f4c85acc8d0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1edbc52-2251-475f-bae1-918e6f000d75","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"38b5c266-7214-4002-888d-10db8c69b75e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0a2a0fdc-fcf1-4b87-b139-47e4c0b498a5","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4c350884-559d-4cb0-acc2-b40722773a01","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2f0f276c-297a-4710-beaa-9777cffba5e1","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b407dcf5-c9b2-43b0-a9cf-16753d82be83","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9fe7eb4-3304-417c-bb96-782675805682","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ac1a739-62fa-4f5a-b9a0-2a8c72d378cd","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b693345-0481-438e-bfbb-e634ac22edf6","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a10b5412-945e-4588-87a6-f17067791034","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbc2226f-65fe-43d0-ad2e-b46cd50cc7a7","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2db0b620-11f6-489c-831d-de18360d8aef","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"74c8edec-f167-407c-b802-a0beee6733a5","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22bd4525-02b2-48db-82e6-006d638046d0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"32a92326-e1b7-4a55-9efa-b481a60204ba","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a34e99d6-12a4-4634-8972-9ccfc50a654d","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81fc1ac3-7721-4f50-ad55-c20921b59ecd","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fca1331c-6321-4142-9d39-5bced097878e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"99550f22-d8ed-46d7-b0e7-0409cf13a9b8","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a7cae5b7-84e2-43a7-9020-dcfbc3d437a0","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"62a0a73d-9311-450d-b29d-5f8685b6b9d1","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af367f12-8b68-409c-9275-a0bd128b143c","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"432bc29a-dd0e-441f-ad5b-5febda65a115","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb2aaa36-b55e-49d8-be91-c9f39e540943","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"49c95672-58bc-4546-8388-83b1ce8f7dd2","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec775f37-5846-4cc5-a7be-bd28d1edda7f","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82a4aebb-9f5a-45a4-85c8-4840e870a9f9","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b80d2861-26cd-4960-a7c4-a56b5fa0d3e7","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5388f7bf-b837-437c-a37f-ccd72069a853","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"15418c0a-4169-4847-8ea8-70a1b4850db4","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f3103839-3970-4d12-adfd-1bf1edbf0af7","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0f7e0902-5a4d-47ee-84fc-b72e8db0fe54","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9c04c30-1f32-4cc8-88ac-982e4777ba9a","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c5b2cd0e-e74c-46c2-b037-e38df2f3b42e","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e667e20-773b-4396-8d15-53eeaca982e7","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31ebc5f0-8207-4c6b-959f-a968bf98f8a5","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ad49bf3-779c-48ec-9448-8214999f7176","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8b9eed93-a820-48dd-a357-829361feab4a","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bdbc5b8e-d48a-4f61-9b38-af3f94f6ce1f","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc5ea591-ef9f-457b-9685-4d90c79d0ae8","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"88c3ca43-4c57-4aa2-aa61-2e101a45e27a","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d0103681-8a6f-4401-90a5-3816a555da4b","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2f12be0-1ab9-4f88-8756-8a1add59fe01","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dfa8f9d7-f988-4e1c-ad5e-3c15a4b9c502","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f400c9f2-6796-47ca-ac9e-530c77464faa","Key":34.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f28d968-3c51-407b-958c-55c8cebcf03f","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"089bf633-1cfc-4f3b-8c95-1a5b633b7d2b","Key":35.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e2b676b-57a5-46cc-b0f2-e42ee58e9db2","path":"sprites/sGoldBoots/sGoldBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sGoldBoots","path":"sprites/sGoldBoots/sGoldBoots.yy",},
    "resourceVersion": "1.3",
    "name": "sGoldBoots",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3d04a456-7790-4370-9ed3-b2385cf8b403","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Equipment",
    "path": "folders/Sprites/Player Sprites/Equipment.yy",
  },
  "resourceVersion": "1.0",
  "name": "sGoldBoots",
  "tags": [],
  "resourceType": "GMSprite",
}