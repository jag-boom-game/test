{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 16,
  "bbox_top": 5,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 18,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e2ff69ea-c526-4040-8859-a7e753101b56","path":"sprites/sWolf/sWolf.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2ff69ea-c526-4040-8859-a7e753101b56","path":"sprites/sWolf/sWolf.yy",},"LayerId":{"name":"cf893cf4-f511-45cf-bb06-c7c6eab5813c","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolf","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"e2ff69ea-c526-4040-8859-a7e753101b56","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2d282f2c-4573-4403-a559-5ac293eb4237","path":"sprites/sWolf/sWolf.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2d282f2c-4573-4403-a559-5ac293eb4237","path":"sprites/sWolf/sWolf.yy",},"LayerId":{"name":"cf893cf4-f511-45cf-bb06-c7c6eab5813c","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolf","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"2d282f2c-4573-4403-a559-5ac293eb4237","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a49f5fb0-8346-4848-affa-ecf58f6fe976","path":"sprites/sWolf/sWolf.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a49f5fb0-8346-4848-affa-ecf58f6fe976","path":"sprites/sWolf/sWolf.yy",},"LayerId":{"name":"cf893cf4-f511-45cf-bb06-c7c6eab5813c","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolf","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"a49f5fb0-8346-4848-affa-ecf58f6fe976","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b86444de-3ebd-4a6e-82e0-e5dba73dde71","path":"sprites/sWolf/sWolf.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b86444de-3ebd-4a6e-82e0-e5dba73dde71","path":"sprites/sWolf/sWolf.yy",},"LayerId":{"name":"cf893cf4-f511-45cf-bb06-c7c6eab5813c","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolf","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","name":"b86444de-3ebd-4a6e-82e0-e5dba73dde71","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sWolf","path":"sprites/sWolf/sWolf.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fc63611b-5b9e-4ed3-a6e4-d2a31ef5ea97","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2ff69ea-c526-4040-8859-a7e753101b56","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"10402729-71cb-4500-be1a-934876254793","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2d282f2c-4573-4403-a559-5ac293eb4237","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3fbe72fe-2468-47c5-a5c5-2f6e81f81bb3","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a49f5fb0-8346-4848-affa-ecf58f6fe976","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e8831470-acfb-4271-aa4e-e64907c3013f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b86444de-3ebd-4a6e-82e0-e5dba73dde71","path":"sprites/sWolf/sWolf.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sWolf","path":"sprites/sWolf/sWolf.yy",},
    "resourceVersion": "1.3",
    "name": "sWolf",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cf893cf4-f511-45cf-bb06-c7c6eab5813c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sWolf",
  "tags": [],
  "resourceType": "GMSprite",
}