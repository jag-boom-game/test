{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 18,
  "bbox_right": 45,
  "bbox_top": 9,
  "bbox_bottom": 37,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5243ba47-9139-43e8-92ef-a87458058347","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5243ba47-9139-43e8-92ef-a87458058347","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"5243ba47-9139-43e8-92ef-a87458058347","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc2fb431-ed98-4dc7-b845-bfc76e9404c2","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc2fb431-ed98-4dc7-b845-bfc76e9404c2","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"cc2fb431-ed98-4dc7-b845-bfc76e9404c2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"628c1797-2c6b-4271-96dc-7f86150cefdf","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"628c1797-2c6b-4271-96dc-7f86150cefdf","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"628c1797-2c6b-4271-96dc-7f86150cefdf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af529762-89ba-4733-8e83-15f016ba4dfa","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af529762-89ba-4733-8e83-15f016ba4dfa","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"af529762-89ba-4733-8e83-15f016ba4dfa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"58adb1cb-50d1-4031-9ea8-3da345bca504","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"58adb1cb-50d1-4031-9ea8-3da345bca504","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"58adb1cb-50d1-4031-9ea8-3da345bca504","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"901bfe20-0766-4d3d-9869-40f83b0849a5","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"901bfe20-0766-4d3d-9869-40f83b0849a5","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"901bfe20-0766-4d3d-9869-40f83b0849a5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7e31005f-4997-42f0-8017-3e80ce713e1a","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7e31005f-4997-42f0-8017-3e80ce713e1a","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"7e31005f-4997-42f0-8017-3e80ce713e1a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ee24fcd-8438-47ce-954c-b765bdde4482","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ee24fcd-8438-47ce-954c-b765bdde4482","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"8ee24fcd-8438-47ce-954c-b765bdde4482","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca698f06-d2de-4872-9c62-a5cd7a1c0e20","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca698f06-d2de-4872-9c62-a5cd7a1c0e20","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"ca698f06-d2de-4872-9c62-a5cd7a1c0e20","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"944eef6b-900b-4672-92b6-1030618caa9e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"944eef6b-900b-4672-92b6-1030618caa9e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"944eef6b-900b-4672-92b6-1030618caa9e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43c89a64-50b4-449d-b164-5bf079229b8d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43c89a64-50b4-449d-b164-5bf079229b8d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"43c89a64-50b4-449d-b164-5bf079229b8d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06e5c317-8f7b-4503-aade-8c0f696698df","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06e5c317-8f7b-4503-aade-8c0f696698df","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"06e5c317-8f7b-4503-aade-8c0f696698df","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12bf993d-be47-4d56-ab56-22e6b6dee67e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12bf993d-be47-4d56-ab56-22e6b6dee67e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"12bf993d-be47-4d56-ab56-22e6b6dee67e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7e030fee-c7b3-4494-9048-1acdd584c412","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7e030fee-c7b3-4494-9048-1acdd584c412","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"7e030fee-c7b3-4494-9048-1acdd584c412","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22984736-492b-4375-b0b8-0676ba8e04ac","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22984736-492b-4375-b0b8-0676ba8e04ac","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"22984736-492b-4375-b0b8-0676ba8e04ac","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6f5ac016-1f60-466b-9820-2753fbfbd57d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6f5ac016-1f60-466b-9820-2753fbfbd57d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"6f5ac016-1f60-466b-9820-2753fbfbd57d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7290c6b9-bf41-409d-a2b7-b2b2a658bd92","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7290c6b9-bf41-409d-a2b7-b2b2a658bd92","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"7290c6b9-bf41-409d-a2b7-b2b2a658bd92","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1989df9f-ad99-409e-8284-3a78e63eda3d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1989df9f-ad99-409e-8284-3a78e63eda3d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"1989df9f-ad99-409e-8284-3a78e63eda3d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f66168d2-754f-40cf-af28-61b56db9654b","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f66168d2-754f-40cf-af28-61b56db9654b","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"f66168d2-754f-40cf-af28-61b56db9654b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9ae84917-d25a-42ec-89a0-45bd050e07a9","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9ae84917-d25a-42ec-89a0-45bd050e07a9","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"9ae84917-d25a-42ec-89a0-45bd050e07a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4d35647c-db16-491a-b5fc-0fead1bc8472","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4d35647c-db16-491a-b5fc-0fead1bc8472","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"4d35647c-db16-491a-b5fc-0fead1bc8472","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f50f2a44-f8a7-4e4e-85fc-806ef08ac29a","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f50f2a44-f8a7-4e4e-85fc-806ef08ac29a","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"f50f2a44-f8a7-4e4e-85fc-806ef08ac29a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a628ceb-5f96-46ce-ba2d-495d318d6079","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a628ceb-5f96-46ce-ba2d-495d318d6079","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"4a628ceb-5f96-46ce-ba2d-495d318d6079","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"95af7354-53e2-4b62-ae6a-76807f2e6079","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"95af7354-53e2-4b62-ae6a-76807f2e6079","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"95af7354-53e2-4b62-ae6a-76807f2e6079","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a18254ee-54c2-477b-8f06-b5dbae4a3f13","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a18254ee-54c2-477b-8f06-b5dbae4a3f13","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"a18254ee-54c2-477b-8f06-b5dbae4a3f13","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"834bce1c-b7ea-4990-8c25-6aaa8d9ed26d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"834bce1c-b7ea-4990-8c25-6aaa8d9ed26d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"834bce1c-b7ea-4990-8c25-6aaa8d9ed26d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22a95de9-8fb5-451e-b42e-9898c50432ea","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22a95de9-8fb5-451e-b42e-9898c50432ea","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"22a95de9-8fb5-451e-b42e-9898c50432ea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2c0a788-1340-4b6c-8bd2-76334346f293","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2c0a788-1340-4b6c-8bd2-76334346f293","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"e2c0a788-1340-4b6c-8bd2-76334346f293","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"192c387c-a589-4d57-a985-1dc9dd734733","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"192c387c-a589-4d57-a985-1dc9dd734733","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"192c387c-a589-4d57-a985-1dc9dd734733","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4137b5f6-3e7b-4b28-a1a1-33be7dede3de","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4137b5f6-3e7b-4b28-a1a1-33be7dede3de","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"4137b5f6-3e7b-4b28-a1a1-33be7dede3de","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0db5657f-3809-4148-a8e4-1022cdedd769","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0db5657f-3809-4148-a8e4-1022cdedd769","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"0db5657f-3809-4148-a8e4-1022cdedd769","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a01a9f28-73cf-421f-be1f-bc9367b77c54","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a01a9f28-73cf-421f-be1f-bc9367b77c54","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"a01a9f28-73cf-421f-be1f-bc9367b77c54","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a627fddf-dc8e-4e58-8e67-adef3b8d014e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a627fddf-dc8e-4e58-8e67-adef3b8d014e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"a627fddf-dc8e-4e58-8e67-adef3b8d014e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"87a84a37-5b1a-400f-8fb8-a0284981dc30","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"87a84a37-5b1a-400f-8fb8-a0284981dc30","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"87a84a37-5b1a-400f-8fb8-a0284981dc30","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6209ce7-adef-4e3b-b9f6-7cdfa51c1222","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6209ce7-adef-4e3b-b9f6-7cdfa51c1222","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"e6209ce7-adef-4e3b-b9f6-7cdfa51c1222","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3d3aae12-b07d-496e-a4e0-fe17b5e10f1b","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3d3aae12-b07d-496e-a4e0-fe17b5e10f1b","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"LayerId":{"name":"27752202-1966-42f8-8825-f6db3f7a2997","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","name":"3d3aae12-b07d-496e-a4e0-fe17b5e10f1b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 36.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"31a7b2a2-8a91-4c04-b84b-ff50f28f986d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5243ba47-9139-43e8-92ef-a87458058347","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b111a3dc-9d20-4095-81ca-d7b70b96193a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc2fb431-ed98-4dc7-b845-bfc76e9404c2","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e1430e8a-4362-4659-9b7e-8af2b29e99e8","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"628c1797-2c6b-4271-96dc-7f86150cefdf","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4dd58a19-80b7-4af8-9a15-3cf54e13a41a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af529762-89ba-4733-8e83-15f016ba4dfa","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a8134544-2558-4e52-9a4d-08d1fb00d0f4","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"58adb1cb-50d1-4031-9ea8-3da345bca504","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"022717fd-ec11-4dff-a110-2b93047d66af","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"901bfe20-0766-4d3d-9869-40f83b0849a5","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c954c94-3428-4f33-9e01-2d026482a63b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7e31005f-4997-42f0-8017-3e80ce713e1a","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e45bc35c-218f-4270-8a60-335c9c9ff5ea","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ee24fcd-8438-47ce-954c-b765bdde4482","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"97c6a2e4-730b-46c4-a2ec-a348f9382cf3","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca698f06-d2de-4872-9c62-a5cd7a1c0e20","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"20ab8e6b-ec74-4cba-84fd-d0b61de78ae6","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"944eef6b-900b-4672-92b6-1030618caa9e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b754549-9ac5-48be-a227-8f6a457b3c39","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43c89a64-50b4-449d-b164-5bf079229b8d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"52ef7061-02a6-4b67-bcc6-1f896a5da7d9","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06e5c317-8f7b-4503-aade-8c0f696698df","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f4c83291-75ff-4562-a448-4d726fd96642","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12bf993d-be47-4d56-ab56-22e6b6dee67e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"469bb8e8-ca50-441b-85b7-9f258066e46e","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7e030fee-c7b3-4494-9048-1acdd584c412","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7147873c-adfd-43b9-a0b2-b638d90bcb4d","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22984736-492b-4375-b0b8-0676ba8e04ac","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4246f8fb-04cd-41d5-963a-a5125209e1d5","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6f5ac016-1f60-466b-9820-2753fbfbd57d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48ec07e6-80b4-4c4b-9d9d-4f2f5790ca38","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7290c6b9-bf41-409d-a2b7-b2b2a658bd92","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ce788eb0-2fd2-440b-9809-9f339f6e59ca","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1989df9f-ad99-409e-8284-3a78e63eda3d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c7a5600e-6454-40ff-9ae0-29eddf0b5f62","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f66168d2-754f-40cf-af28-61b56db9654b","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dcee35dd-c494-49f3-9f34-9949ce982d88","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9ae84917-d25a-42ec-89a0-45bd050e07a9","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"59f32726-be49-41b5-820f-a920cb781cf6","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d35647c-db16-491a-b5fc-0fead1bc8472","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"99e5b383-6dc7-4e10-b1cc-673d2953975b","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f50f2a44-f8a7-4e4e-85fc-806ef08ac29a","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"45d5d699-9795-450b-b095-75b332793bc1","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a628ceb-5f96-46ce-ba2d-495d318d6079","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afd18844-c38e-4b4e-8135-ed131eee2426","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"95af7354-53e2-4b62-ae6a-76807f2e6079","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a4ad24af-c0de-4638-956a-be798ef5cf93","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a18254ee-54c2-477b-8f06-b5dbae4a3f13","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b4cabbad-dc84-4153-8799-9288bbefa093","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"834bce1c-b7ea-4990-8c25-6aaa8d9ed26d","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fcaf5cd1-5f8b-4dba-b8ab-aef684be8627","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22a95de9-8fb5-451e-b42e-9898c50432ea","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc657ea5-38bd-40b9-ab2f-8584c0b6dbc3","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2c0a788-1340-4b6c-8bd2-76334346f293","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0d6b3470-2f45-4b9b-a669-2446d83b4796","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"192c387c-a589-4d57-a985-1dc9dd734733","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d5ec8e6-ad17-403f-82b2-f250bd9c2f74","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4137b5f6-3e7b-4b28-a1a1-33be7dede3de","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6959cab-829e-49d0-ace4-ed765b31b761","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0db5657f-3809-4148-a8e4-1022cdedd769","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7a4c37b7-8e86-4c12-a0ea-eededc7359bf","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a01a9f28-73cf-421f-be1f-bc9367b77c54","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"98c0b812-8019-43e7-bf05-7b3b984ce3da","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a627fddf-dc8e-4e58-8e67-adef3b8d014e","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8164d4da-6043-4614-a0b5-6cd211a833b9","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"87a84a37-5b1a-400f-8fb8-a0284981dc30","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"75a0f423-ea3f-4a44-b96a-154048ef5574","Key":34.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6209ce7-adef-4e3b-b9f6-7cdfa51c1222","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1d18b9d-b689-45e8-83ca-223eb8542918","Key":35.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3d3aae12-b07d-496e-a4e0-fe17b5e10f1b","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sGoldHelmet","path":"sprites/sGoldHelmet/sGoldHelmet.yy",},
    "resourceVersion": "1.3",
    "name": "sGoldHelmet",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"27752202-1966-42f8-8825-f6db3f7a2997","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Equipment",
    "path": "folders/Sprites/Player Sprites/Equipment.yy",
  },
  "resourceVersion": "1.0",
  "name": "sGoldHelmet",
  "tags": [],
  "resourceType": "GMSprite",
}