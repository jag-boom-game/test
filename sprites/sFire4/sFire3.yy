{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 48,
  "bbox_top": 4,
  "bbox_bottom": 46,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 50,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"8007fe30-36b8-4f06-b9f0-29e26409b329","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"f2634179-6afe-4558-a0fe-59583a34fdb3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"6a587e9f-e662-4aab-892c-922bc389b654","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"e28e15ab-b525-4041-a15e-d98c75c5023b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"53248431-2c24-474f-8f20-761fe46a964c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"d6891f86-171e-427f-b6f5-c8691990576c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"88a122ea-e599-4658-8938-116cbc1cfa97","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"e4a16cdd-9954-49bc-880b-667847d9c687","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"086145eb-f538-4f8b-a21d-92153df4c08b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"37ffb947-8118-4406-a619-98857c1e3da1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"1197df5c-5b12-480b-9b53-4572faa75647","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"7a30241b-a29c-481c-b042-5cb97aa41e79","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire3/sFire3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire3/sFire3.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 25.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8e32309d-c583-4c57-a185-02901b95b2f8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bf8b097e-121a-4f24-8fbb-6e0f4c4ba77e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5cbf837b-3d9f-435e-bac3-3fdc164bb7fe","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"16c8b2b9-0fb1-4f02-85a0-35326c0a901b","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f46c4b7c-cde9-439a-b050-8c5a7b9a5bf2","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ff599c95-cb56-461b-8fd4-754b7ca5f00b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ca079aa-e8bc-4002-920b-914ee1dfcb23","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1afcdc9d-029d-49fb-b964-b9281ecf97c4","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f6f021d-8ca4-4085-b4c2-14c6da9ecf9e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e28e104-6853-4f96-9c0c-d514b399ef78","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bd10bd90-c8eb-47e0-8859-f803e99266a2","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8751eb3-e38c-423e-8518-d8368c55b3cf","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d64163e3-2416-45a7-9aa6-7c38a7794b92","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aeb652d7-ca9f-4504-b96f-135e47001186","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9c26d6aa-f5b5-48f3-9085-074bde5c6788","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"20c74414-e601-491c-b5b4-081d7e883d98","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"08cda8ce-bed4-4a18-b05b-64ea6207046d","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6306eca5-c151-4abd-a448-a216c38f5144","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"70cb4a56-36e5-4ed3-8143-e7528f5b9d8d","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"94c08123-c4c8-40b3-b2f8-6aee87eb2a9d","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f4207360-07a7-412e-8869-04db8ca22905","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8aabfb1c-83f7-4a57-b767-88a07379b719","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"73bddb78-6f96-4cd9-a580-64c664557812","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"50f62366-0204-45f7-a3bf-f183db8ab144","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"65b3065c-b832-4e40-82bc-bd28c619b841","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire3/sFire3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sFire3","path":"sprites/sFire3/sFire3.yy",},
    "resourceVersion": "1.3",
    "name": "sFire3",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"327edc59-5bab-443c-84bc-55680646e89c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sFire3",
  "tags": [],
  "resourceType": "GMSprite",
}