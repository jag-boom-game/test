{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 10,
  "bbox_right": 53,
  "bbox_top": 6,
  "bbox_bottom": 36,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"985bf8ec-4f71-4d88-9304-1c4a007b9ac7","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"985bf8ec-4f71-4d88-9304-1c4a007b9ac7","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"985bf8ec-4f71-4d88-9304-1c4a007b9ac7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1b83b353-aca3-464c-b212-c158eb1955ac","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b83b353-aca3-464c-b212-c158eb1955ac","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"1b83b353-aca3-464c-b212-c158eb1955ac","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca638174-6b37-46d6-8aac-9ebce6843205","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca638174-6b37-46d6-8aac-9ebce6843205","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"ca638174-6b37-46d6-8aac-9ebce6843205","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3d59cf7e-ab3c-45e6-b826-16b1696a8507","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3d59cf7e-ab3c-45e6-b826-16b1696a8507","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"3d59cf7e-ab3c-45e6-b826-16b1696a8507","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4efde31a-2734-4b67-8be5-538cc5cc8d78","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4efde31a-2734-4b67-8be5-538cc5cc8d78","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"4efde31a-2734-4b67-8be5-538cc5cc8d78","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f6ca42fc-7648-421c-84bf-53174a35d9c5","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f6ca42fc-7648-421c-84bf-53174a35d9c5","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"f6ca42fc-7648-421c-84bf-53174a35d9c5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2cc5a15f-d85f-4de0-af37-a9639688c308","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2cc5a15f-d85f-4de0-af37-a9639688c308","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"2cc5a15f-d85f-4de0-af37-a9639688c308","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4618e364-5014-42f7-a1a8-873b8bf53eaf","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4618e364-5014-42f7-a1a8-873b8bf53eaf","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"4618e364-5014-42f7-a1a8-873b8bf53eaf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"83832aff-fe77-43a3-b58d-f5d5aba7b43b","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"83832aff-fe77-43a3-b58d-f5d5aba7b43b","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"83832aff-fe77-43a3-b58d-f5d5aba7b43b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f677c300-9ed0-470c-8019-61f3d03e4e7d","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f677c300-9ed0-470c-8019-61f3d03e4e7d","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"f677c300-9ed0-470c-8019-61f3d03e4e7d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"14453d55-91e8-4991-9a11-e4bc1c3fd684","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"14453d55-91e8-4991-9a11-e4bc1c3fd684","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"14453d55-91e8-4991-9a11-e4bc1c3fd684","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eea11e33-3ae6-4a8d-95ec-2553a55a6521","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eea11e33-3ae6-4a8d-95ec-2553a55a6521","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"eea11e33-3ae6-4a8d-95ec-2553a55a6521","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a642d470-234e-476d-9630-7428cbfcbe6a","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a642d470-234e-476d-9630-7428cbfcbe6a","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"a642d470-234e-476d-9630-7428cbfcbe6a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43949788-a933-4894-b9c3-f9dca6f01ec8","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43949788-a933-4894-b9c3-f9dca6f01ec8","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"43949788-a933-4894-b9c3-f9dca6f01ec8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"98533dcf-a146-458e-8396-6b98d89ab97a","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98533dcf-a146-458e-8396-6b98d89ab97a","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"98533dcf-a146-458e-8396-6b98d89ab97a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ea9da910-7f9b-4c42-a732-aeeb1b442659","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea9da910-7f9b-4c42-a732-aeeb1b442659","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"ea9da910-7f9b-4c42-a732-aeeb1b442659","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fdebfcfb-c5a8-4e52-b066-52ca93e5ba8b","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fdebfcfb-c5a8-4e52-b066-52ca93e5ba8b","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"fdebfcfb-c5a8-4e52-b066-52ca93e5ba8b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ffb73dc1-ffd0-4a90-988b-8db38abeb1cc","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ffb73dc1-ffd0-4a90-988b-8db38abeb1cc","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"ffb73dc1-ffd0-4a90-988b-8db38abeb1cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34342bd0-5f0a-4ef0-9a26-ac0e37a9f6e2","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34342bd0-5f0a-4ef0-9a26-ac0e37a9f6e2","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"34342bd0-5f0a-4ef0-9a26-ac0e37a9f6e2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f3759640-3549-4cd4-b838-b06d79a512b0","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f3759640-3549-4cd4-b838-b06d79a512b0","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"f3759640-3549-4cd4-b838-b06d79a512b0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d92ebc39-c61d-45d1-9081-a19653c056ef","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d92ebc39-c61d-45d1-9081-a19653c056ef","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"d92ebc39-c61d-45d1-9081-a19653c056ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e75ecaae-46ca-4e7a-b9ae-c9e19218ce94","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e75ecaae-46ca-4e7a-b9ae-c9e19218ce94","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"e75ecaae-46ca-4e7a-b9ae-c9e19218ce94","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a0f55a94-2a3d-4fe8-a28f-a0413c7bdb13","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a0f55a94-2a3d-4fe8-a28f-a0413c7bdb13","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"a0f55a94-2a3d-4fe8-a28f-a0413c7bdb13","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ed3d4125-f2e1-41de-9e26-be32a47550b6","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ed3d4125-f2e1-41de-9e26-be32a47550b6","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"ed3d4125-f2e1-41de-9e26-be32a47550b6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f6c95538-56b4-45d8-9abc-4baabc894cfa","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f6c95538-56b4-45d8-9abc-4baabc894cfa","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"f6c95538-56b4-45d8-9abc-4baabc894cfa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5458db84-80ce-49c3-bed3-82f796b3b271","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5458db84-80ce-49c3-bed3-82f796b3b271","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"5458db84-80ce-49c3-bed3-82f796b3b271","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c0e2bdc-1b18-4b94-bb76-8a64c19e4584","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c0e2bdc-1b18-4b94-bb76-8a64c19e4584","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"5c0e2bdc-1b18-4b94-bb76-8a64c19e4584","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6e8b2156-3dcd-4160-a32a-139921f85df9","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6e8b2156-3dcd-4160-a32a-139921f85df9","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"6e8b2156-3dcd-4160-a32a-139921f85df9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2a459bad-b775-4188-949d-3a522cac3e2e","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2a459bad-b775-4188-949d-3a522cac3e2e","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"2a459bad-b775-4188-949d-3a522cac3e2e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9ccbdaf0-50d9-45ad-9f57-82fec3c8e9a4","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9ccbdaf0-50d9-45ad-9f57-82fec3c8e9a4","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"9ccbdaf0-50d9-45ad-9f57-82fec3c8e9a4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aba8842f-90e0-471b-9369-e0878137e4da","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aba8842f-90e0-471b-9369-e0878137e4da","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"aba8842f-90e0-471b-9369-e0878137e4da","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"31ad0370-be9d-45ba-a056-0107e697dadd","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31ad0370-be9d-45ba-a056-0107e697dadd","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"31ad0370-be9d-45ba-a056-0107e697dadd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af859d6d-8d3f-4b5f-9cc8-a80a7c787a77","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af859d6d-8d3f-4b5f-9cc8-a80a7c787a77","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"af859d6d-8d3f-4b5f-9cc8-a80a7c787a77","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70a42fd7-0b94-4ffa-9f2e-12462127c85d","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70a42fd7-0b94-4ffa-9f2e-12462127c85d","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"70a42fd7-0b94-4ffa-9f2e-12462127c85d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3e9c84b7-d774-40ed-b638-41953761d2ba","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3e9c84b7-d774-40ed-b638-41953761d2ba","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"3e9c84b7-d774-40ed-b638-41953761d2ba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"56aca0ce-8eaf-49b0-92cf-7aff7b6c98d7","path":"sprites/sCap/sCap.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"56aca0ce-8eaf-49b0-92cf-7aff7b6c98d7","path":"sprites/sCap/sCap.yy",},"LayerId":{"name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sCap","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","name":"56aca0ce-8eaf-49b0-92cf-7aff7b6c98d7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sCap","path":"sprites/sCap/sCap.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 36.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ef3117e6-98dd-44b8-822b-0f6386ca8934","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"985bf8ec-4f71-4d88-9304-1c4a007b9ac7","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e68cebf6-2f2b-4635-aa2b-15d187e7d187","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b83b353-aca3-464c-b212-c158eb1955ac","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06be9883-0519-4921-aa3e-fa727e7131e3","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca638174-6b37-46d6-8aac-9ebce6843205","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"470c410e-dcc6-43bf-95cd-92b98c6f47d9","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3d59cf7e-ab3c-45e6-b826-16b1696a8507","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"17199b76-8c0e-4ff2-a9ea-ebff3029d715","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4efde31a-2734-4b67-8be5-538cc5cc8d78","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"664db69f-9aff-4e07-827c-4027be683087","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f6ca42fc-7648-421c-84bf-53174a35d9c5","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f40cb92a-64b4-4f60-b106-03c8eebd5e6c","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2cc5a15f-d85f-4de0-af37-a9639688c308","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbfb1eab-6bdf-4433-8b9b-fb41bbd130d4","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4618e364-5014-42f7-a1a8-873b8bf53eaf","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6521bf35-35fe-46c9-b51d-17cac4c8d6e0","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"83832aff-fe77-43a3-b58d-f5d5aba7b43b","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06731eb3-eef5-4f64-8803-b9777c58d9a5","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f677c300-9ed0-470c-8019-61f3d03e4e7d","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2e9a0090-2c32-4211-9366-614449b2b1de","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14453d55-91e8-4991-9a11-e4bc1c3fd684","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f2755b3b-105c-49ad-8520-3ed3fc64c248","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eea11e33-3ae6-4a8d-95ec-2553a55a6521","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5352c451-bacf-48b5-9a5f-fb05e08d33d8","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a642d470-234e-476d-9630-7428cbfcbe6a","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"439f75df-08bf-4e30-894d-43f2fc2c475a","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43949788-a933-4894-b9c3-f9dca6f01ec8","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"157523aa-c538-451f-a79e-fe2b477d28af","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98533dcf-a146-458e-8396-6b98d89ab97a","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f5251221-9a12-402a-ad59-db594ceaf411","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea9da910-7f9b-4c42-a732-aeeb1b442659","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b4b8b41b-5bf1-410c-9174-786236c47078","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fdebfcfb-c5a8-4e52-b066-52ca93e5ba8b","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ebe22433-3996-4191-a962-ff0143a55df6","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ffb73dc1-ffd0-4a90-988b-8db38abeb1cc","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b7fec777-1de0-4caf-a19b-3979e6b92afe","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34342bd0-5f0a-4ef0-9a26-ac0e37a9f6e2","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bd04e14a-a44f-4701-9a1f-220b75844224","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f3759640-3549-4cd4-b838-b06d79a512b0","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39b9c851-bec8-4226-bf3b-33fb27cbfe5a","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d92ebc39-c61d-45d1-9081-a19653c056ef","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b794896b-91d3-4983-84c6-663ab1557bcf","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e75ecaae-46ca-4e7a-b9ae-c9e19218ce94","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"86dabb9f-6d76-420e-a669-2cd4b2270b7a","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a0f55a94-2a3d-4fe8-a28f-a0413c7bdb13","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06c6e748-62c0-4901-b789-11f52d9ae2b0","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed3d4125-f2e1-41de-9e26-be32a47550b6","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bb00ecd7-ec8e-438d-ae43-2e3854c4a6b8","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f6c95538-56b4-45d8-9abc-4baabc894cfa","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"623b254a-335b-46f8-b37b-1ac2ceb9a366","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5458db84-80ce-49c3-bed3-82f796b3b271","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"95cd6848-8322-43cd-8a9b-8dad962adef8","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c0e2bdc-1b18-4b94-bb76-8a64c19e4584","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7b08b733-4c70-483d-8a34-06ce26230fe0","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6e8b2156-3dcd-4160-a32a-139921f85df9","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f2a1ca10-9ab6-4dd4-a060-35fc2a744551","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a459bad-b775-4188-949d-3a522cac3e2e","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5fcf45b9-aa5d-459f-9a5e-ed4f75ae78cf","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9ccbdaf0-50d9-45ad-9f57-82fec3c8e9a4","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00996e46-a778-4c23-b17a-7c43f8f63b15","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aba8842f-90e0-471b-9369-e0878137e4da","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fecff7b5-2b46-4e5d-8f05-16e30d170ef7","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31ad0370-be9d-45ba-a056-0107e697dadd","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2fd66bf8-56f5-4823-8c48-b9d6cde325ce","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af859d6d-8d3f-4b5f-9cc8-a80a7c787a77","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a88c5688-f037-474c-908c-5c4bb6101d1f","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70a42fd7-0b94-4ffa-9f2e-12462127c85d","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c02f1e85-c9d1-4a97-bcf9-58e4f777204a","Key":34.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3e9c84b7-d774-40ed-b638-41953761d2ba","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"118f76cc-0791-4c54-a638-1b6ab12f1134","Key":35.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"56aca0ce-8eaf-49b0-92cf-7aff7b6c98d7","path":"sprites/sCap/sCap.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sCap","path":"sprites/sCap/sCap.yy",},
    "resourceVersion": "1.3",
    "name": "sCap",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0b8bb255-79e6-40c2-a7cd-4b36913786a9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Equipment",
    "path": "folders/Sprites/Player Sprites/Equipment.yy",
  },
  "resourceVersion": "1.0",
  "name": "sCap",
  "tags": [],
  "resourceType": "GMSprite",
}