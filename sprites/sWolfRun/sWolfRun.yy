{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 16,
  "bbox_top": 4,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 18,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"580d2b56-0844-47a2-af58-c7b47abbf8d4","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"580d2b56-0844-47a2-af58-c7b47abbf8d4","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":{"name":"fa92f059-fe9a-48ca-8ec2-48a930acc35e","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolfRun","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"580d2b56-0844-47a2-af58-c7b47abbf8d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"48f6023b-1aff-490e-813c-c69eb976abda","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"48f6023b-1aff-490e-813c-c69eb976abda","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":{"name":"fa92f059-fe9a-48ca-8ec2-48a930acc35e","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolfRun","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"48f6023b-1aff-490e-813c-c69eb976abda","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"58fb011a-fd40-49df-941a-78ed6e1a1841","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"58fb011a-fd40-49df-941a-78ed6e1a1841","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":{"name":"fa92f059-fe9a-48ca-8ec2-48a930acc35e","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolfRun","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"58fb011a-fd40-49df-941a-78ed6e1a1841","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b4ffa2a1-ff7c-4323-b2c3-1130ea2ae56c","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b4ffa2a1-ff7c-4323-b2c3-1130ea2ae56c","path":"sprites/sWolfRun/sWolfRun.yy",},"LayerId":{"name":"fa92f059-fe9a-48ca-8ec2-48a930acc35e","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sWolfRun","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","name":"b4ffa2a1-ff7c-4323-b2c3-1130ea2ae56c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sWolfRun","path":"sprites/sWolfRun/sWolfRun.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e921d5d9-cf5b-480d-8f9d-a30ca27fbdbb","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"580d2b56-0844-47a2-af58-c7b47abbf8d4","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ad2feffa-e1df-4d38-97b7-274d2e230c2a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"48f6023b-1aff-490e-813c-c69eb976abda","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"26754a25-0eec-4c9f-ba36-ee267681ff04","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"58fb011a-fd40-49df-941a-78ed6e1a1841","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c5f66afe-54da-4d5d-bf5e-6db421293f13","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4ffa2a1-ff7c-4323-b2c3-1130ea2ae56c","path":"sprites/sWolfRun/sWolfRun.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sWolfRun","path":"sprites/sWolfRun/sWolfRun.yy",},
    "resourceVersion": "1.3",
    "name": "sWolfRun",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"fa92f059-fe9a-48ca-8ec2-48a930acc35e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sWolfRun",
  "tags": [],
  "resourceType": "GMSprite",
}