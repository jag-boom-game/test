{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 19,
  "bbox_right": 44,
  "bbox_top": 43,
  "bbox_bottom": 62,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3a67d39f-b89b-4ab7-8c03-906cc9542f0d","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3a67d39f-b89b-4ab7-8c03-906cc9542f0d","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"3a67d39f-b89b-4ab7-8c03-906cc9542f0d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6f51822c-e72d-4302-9e49-d814cbb303b9","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6f51822c-e72d-4302-9e49-d814cbb303b9","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"6f51822c-e72d-4302-9e49-d814cbb303b9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49201f64-9d80-45a0-bd62-dd6f2099ec19","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49201f64-9d80-45a0-bd62-dd6f2099ec19","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"49201f64-9d80-45a0-bd62-dd6f2099ec19","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"37338257-f2b6-4023-b862-18bd30aa333c","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"37338257-f2b6-4023-b862-18bd30aa333c","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"37338257-f2b6-4023-b862-18bd30aa333c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e39e0e6b-49af-4692-8d3c-5a40ade773ae","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e39e0e6b-49af-4692-8d3c-5a40ade773ae","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"e39e0e6b-49af-4692-8d3c-5a40ade773ae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dfc87a62-15a2-4676-b652-50cf5c789690","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dfc87a62-15a2-4676-b652-50cf5c789690","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"dfc87a62-15a2-4676-b652-50cf5c789690","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c0430caa-e864-4da6-b95c-7afe85a93c97","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c0430caa-e864-4da6-b95c-7afe85a93c97","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"c0430caa-e864-4da6-b95c-7afe85a93c97","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9245d25d-2503-4b88-816e-9f8daa53220e","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9245d25d-2503-4b88-816e-9f8daa53220e","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"9245d25d-2503-4b88-816e-9f8daa53220e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"531f9aee-e3f3-4383-ad40-2ba5d9a6d4aa","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"531f9aee-e3f3-4383-ad40-2ba5d9a6d4aa","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"531f9aee-e3f3-4383-ad40-2ba5d9a6d4aa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"617b4d63-a51b-4034-86d7-57a599edce83","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"617b4d63-a51b-4034-86d7-57a599edce83","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"617b4d63-a51b-4034-86d7-57a599edce83","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22c7c5be-10cc-4efd-b8df-6842803c6a4c","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22c7c5be-10cc-4efd-b8df-6842803c6a4c","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"22c7c5be-10cc-4efd-b8df-6842803c6a4c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"179d9500-3787-434a-be31-8f006b5920fa","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"179d9500-3787-434a-be31-8f006b5920fa","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"179d9500-3787-434a-be31-8f006b5920fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"29ede7c5-ec88-4c30-ae97-ba8311fc9d56","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"29ede7c5-ec88-4c30-ae97-ba8311fc9d56","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"29ede7c5-ec88-4c30-ae97-ba8311fc9d56","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0f2954ab-c6af-41c7-8928-1eca260e26e6","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0f2954ab-c6af-41c7-8928-1eca260e26e6","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"0f2954ab-c6af-41c7-8928-1eca260e26e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"19cbab31-98f8-4d57-846d-376b5623efda","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"19cbab31-98f8-4d57-846d-376b5623efda","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"19cbab31-98f8-4d57-846d-376b5623efda","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22aab942-6ea0-45b1-9c39-752cfba4dbb2","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22aab942-6ea0-45b1-9c39-752cfba4dbb2","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"22aab942-6ea0-45b1-9c39-752cfba4dbb2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c18e36f9-be75-4ef1-b050-0914e92ef091","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c18e36f9-be75-4ef1-b050-0914e92ef091","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"c18e36f9-be75-4ef1-b050-0914e92ef091","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"67cc85f7-7c04-40da-bdb9-a94098c9d3a2","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"67cc85f7-7c04-40da-bdb9-a94098c9d3a2","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"67cc85f7-7c04-40da-bdb9-a94098c9d3a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"45a0d51b-b8a5-4a18-b9e7-28d201e861a4","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"45a0d51b-b8a5-4a18-b9e7-28d201e861a4","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"45a0d51b-b8a5-4a18-b9e7-28d201e861a4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6e06bbdc-55bd-4ea2-8147-487e452eafb8","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6e06bbdc-55bd-4ea2-8147-487e452eafb8","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"6e06bbdc-55bd-4ea2-8147-487e452eafb8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"311beaf4-a08b-4f1c-8779-9adbfe6bee6c","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"311beaf4-a08b-4f1c-8779-9adbfe6bee6c","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"311beaf4-a08b-4f1c-8779-9adbfe6bee6c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c3078e2e-f3e3-43e4-84e1-cad8bac164d6","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c3078e2e-f3e3-43e4-84e1-cad8bac164d6","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"c3078e2e-f3e3-43e4-84e1-cad8bac164d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"975b70d5-7de5-4b9c-97ff-517fb16b3fd4","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"975b70d5-7de5-4b9c-97ff-517fb16b3fd4","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"975b70d5-7de5-4b9c-97ff-517fb16b3fd4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77e301d2-7b2b-40b4-89be-d291bd2c97da","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77e301d2-7b2b-40b4-89be-d291bd2c97da","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"77e301d2-7b2b-40b4-89be-d291bd2c97da","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7bc22e25-4e25-4115-bf23-c7fbf966f75a","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7bc22e25-4e25-4115-bf23-c7fbf966f75a","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"7bc22e25-4e25-4115-bf23-c7fbf966f75a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2ded0894-92fc-4d46-9b4a-dbb89f00cd8e","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ded0894-92fc-4d46-9b4a-dbb89f00cd8e","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"2ded0894-92fc-4d46-9b4a-dbb89f00cd8e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d8f69d59-48fd-4c73-876c-2f961a33909c","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8f69d59-48fd-4c73-876c-2f961a33909c","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"d8f69d59-48fd-4c73-876c-2f961a33909c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a14f6c03-d210-4037-b713-19363c30cc38","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a14f6c03-d210-4037-b713-19363c30cc38","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"a14f6c03-d210-4037-b713-19363c30cc38","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"417011b9-7744-4b10-a212-f41fd7f120bb","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"417011b9-7744-4b10-a212-f41fd7f120bb","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"417011b9-7744-4b10-a212-f41fd7f120bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b39bef3-cae8-473e-a6c7-b639f39207c6","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b39bef3-cae8-473e-a6c7-b639f39207c6","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"2b39bef3-cae8-473e-a6c7-b639f39207c6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5915edbf-76d8-4044-a556-70a5ccd3aec3","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5915edbf-76d8-4044-a556-70a5ccd3aec3","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"5915edbf-76d8-4044-a556-70a5ccd3aec3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"032f7f3b-8f87-44fe-8fa7-88574cb0aa87","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"032f7f3b-8f87-44fe-8fa7-88574cb0aa87","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"032f7f3b-8f87-44fe-8fa7-88574cb0aa87","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a6c76442-03f1-4ecc-aa55-b949b2c04287","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a6c76442-03f1-4ecc-aa55-b949b2c04287","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"a6c76442-03f1-4ecc-aa55-b949b2c04287","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fd9a30f4-4c97-4bb5-bf11-6622c94b8faf","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fd9a30f4-4c97-4bb5-bf11-6622c94b8faf","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"fd9a30f4-4c97-4bb5-bf11-6622c94b8faf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d448a718-ece9-4fef-938b-0df02ca72b49","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d448a718-ece9-4fef-938b-0df02ca72b49","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"d448a718-ece9-4fef-938b-0df02ca72b49","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e239e0aa-963f-443f-88a1-5d065ca3a128","path":"sprites/sBoots/sBoots.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e239e0aa-963f-443f-88a1-5d065ca3a128","path":"sprites/sBoots/sBoots.yy",},"LayerId":{"name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","name":"e239e0aa-963f-443f-88a1-5d065ca3a128","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 36.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0e4a426a-8866-4401-a380-b9dded6f4039","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a67d39f-b89b-4ab7-8c03-906cc9542f0d","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"532d8859-b71a-4c26-b81f-ca293de78066","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6f51822c-e72d-4302-9e49-d814cbb303b9","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"efd3f0ea-707b-4588-aa27-8ff1bf9fd167","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49201f64-9d80-45a0-bd62-dd6f2099ec19","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3885b106-418b-4e52-9863-7e5f04ffbfc7","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"37338257-f2b6-4023-b862-18bd30aa333c","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b52d99cf-0c2f-4a86-80ae-9c46f2cd4f52","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e39e0e6b-49af-4692-8d3c-5a40ade773ae","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af06bed9-7a91-4261-bf7f-8d9cc8129601","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dfc87a62-15a2-4676-b652-50cf5c789690","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf3c0f79-3f2e-44f4-a0dd-3609ffbc5212","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c0430caa-e864-4da6-b95c-7afe85a93c97","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"32df2c0b-0835-4094-b907-7434fdd5cf9c","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9245d25d-2503-4b88-816e-9f8daa53220e","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5ed9c4b1-215f-4ba4-9db7-9837526c2624","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"531f9aee-e3f3-4383-ad40-2ba5d9a6d4aa","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6d8d8366-33a6-40b8-82a5-98ed8511c43f","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"617b4d63-a51b-4034-86d7-57a599edce83","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9179bdb4-da5c-4985-898a-0c66975ed113","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22c7c5be-10cc-4efd-b8df-6842803c6a4c","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c42823f8-4a25-4d27-bb60-f992d7044651","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"179d9500-3787-434a-be31-8f006b5920fa","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44472045-94d9-4927-9ba8-1ec23cbd61c0","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29ede7c5-ec88-4c30-ae97-ba8311fc9d56","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a1994a3b-a01c-47af-b5f0-b063d468b7fb","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0f2954ab-c6af-41c7-8928-1eca260e26e6","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"059f74f9-6acb-4ccb-8996-8553829ad8c8","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"19cbab31-98f8-4d57-846d-376b5623efda","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4bca992d-546d-4cf7-bcac-18bc18a2ffa1","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22aab942-6ea0-45b1-9c39-752cfba4dbb2","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4c255dce-4082-43c6-8221-248d65ae8159","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c18e36f9-be75-4ef1-b050-0914e92ef091","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afcfc660-f0aa-4488-ad69-51a55cb0c913","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"67cc85f7-7c04-40da-bdb9-a94098c9d3a2","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"72ed2056-4a4f-43f6-a696-616f57e77cb8","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"45a0d51b-b8a5-4a18-b9e7-28d201e861a4","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"abdf6855-da61-4ad4-8a4f-3fb771f516fd","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6e06bbdc-55bd-4ea2-8147-487e452eafb8","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af91afa2-7728-45bf-b4c1-130afb4a4635","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"311beaf4-a08b-4f1c-8779-9adbfe6bee6c","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f2b9312c-6795-44d2-9619-d0d12b1e1aa1","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c3078e2e-f3e3-43e4-84e1-cad8bac164d6","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"197ba8e9-a1b4-4e8c-aeb5-43e491c95793","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"975b70d5-7de5-4b9c-97ff-517fb16b3fd4","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"80239e08-c39d-4d41-aec6-a2da3679ca7e","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77e301d2-7b2b-40b4-89be-d291bd2c97da","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2753e798-d6e2-4f52-9cca-d5a12063a9ab","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7bc22e25-4e25-4115-bf23-c7fbf966f75a","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a04a3010-cf36-4601-bf1a-9878799478dc","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ded0894-92fc-4d46-9b4a-dbb89f00cd8e","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ab9c4472-c98d-4734-b66e-ad1085f9ab66","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8f69d59-48fd-4c73-876c-2f961a33909c","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31049fbf-5dcb-4d12-837d-86f95d19fe34","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a14f6c03-d210-4037-b713-19363c30cc38","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"97d68343-7b57-43c7-a4cc-bb23f5856267","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"417011b9-7744-4b10-a212-f41fd7f120bb","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc530ce8-72c1-47f2-b5d2-892f3aee4b18","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b39bef3-cae8-473e-a6c7-b639f39207c6","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"37ae40e3-fabd-463c-a66e-a660654127c6","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5915edbf-76d8-4044-a556-70a5ccd3aec3","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"71d9f5e2-52dd-43d3-91df-4ff210b9c8f6","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"032f7f3b-8f87-44fe-8fa7-88574cb0aa87","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"577d8051-84ba-4b94-a5da-8a61cb021f8e","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a6c76442-03f1-4ecc-aa55-b949b2c04287","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22d97d6a-9a45-4732-95db-74f3840eb050","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd9a30f4-4c97-4bb5-bf11-6622c94b8faf","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"860f86c9-c484-46a8-88f7-b989743689d4","Key":34.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d448a718-ece9-4fef-938b-0df02ca72b49","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"566bf598-fbd5-450d-b10e-004ac7946f04","Key":35.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e239e0aa-963f-443f-88a1-5d065ca3a128","path":"sprites/sBoots/sBoots.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sBoots","path":"sprites/sBoots/sBoots.yy",},
    "resourceVersion": "1.3",
    "name": "sBoots",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0172dfc1-5383-4999-8aa8-4db5a69e8c29","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Equipment",
    "path": "folders/Sprites/Player Sprites/Equipment.yy",
  },
  "resourceVersion": "1.0",
  "name": "sBoots",
  "tags": [],
  "resourceType": "GMSprite",
}