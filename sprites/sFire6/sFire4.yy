{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 48,
  "bbox_top": 4,
  "bbox_bottom": 46,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 50,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"8007fe30-36b8-4f06-b9f0-29e26409b329","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"f2634179-6afe-4558-a0fe-59583a34fdb3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"6a587e9f-e662-4aab-892c-922bc389b654","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"e28e15ab-b525-4041-a15e-d98c75c5023b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"53248431-2c24-474f-8f20-761fe46a964c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"d6891f86-171e-427f-b6f5-c8691990576c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"88a122ea-e599-4658-8938-116cbc1cfa97","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"e4a16cdd-9954-49bc-880b-667847d9c687","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"086145eb-f538-4f8b-a21d-92153df4c08b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"37ffb947-8118-4406-a619-98857c1e3da1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"1197df5c-5b12-480b-9b53-4572faa75647","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"7a30241b-a29c-481c-b042-5cb97aa41e79","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire4/sFire4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire4/sFire4.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 25.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d5a81a06-a1c8-49b0-ab18-d488d8c85db4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ac51e525-b871-41a6-971a-751f3c4369a9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"45b166ae-84be-44cf-99e4-05d5812fc7b1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c8f5fbd-ec43-4c09-9a7c-76408c734dc5","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cd261b35-de03-4fca-8b6e-506ac963c8f9","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f0b53e5d-9ef4-4ea0-ab80-ed9de62e8da0","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b89c831-d5f1-4203-98b1-bd8d8465cef7","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"879673d3-a978-4c0b-bda5-6dba6f0342bc","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"62a4c8ae-eff6-41fa-8c2a-a901f25d56e2","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1eecf0e8-1457-44a0-ad30-9526970e8d93","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6e5481f8-2435-4cb4-af8b-bd85f1a81e3a","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"edadc459-391c-4b3a-a885-3cf99b2c3519","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3ff305e-36ba-4910-9148-9de0821915f1","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb950ae7-1a73-4c36-b145-301bbf94d7fb","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d89f7a0-4acc-44f9-96b8-b0a2f2ed7ca0","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9075395-1e5b-4f7a-838b-31136bbb7a19","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed75f679-2ab1-438e-a27d-0b4626c4ab06","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"28bb4db0-3d74-4eed-9fe8-472d025cdb30","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d412ba02-9d7a-482b-9686-1489952fac0c","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8733e850-dcec-489a-a2ea-c3a366a9ffc2","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18cc92f5-b404-438f-bfbd-94c719c96c56","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7bc3e7c5-8f02-4026-8d89-2c1beded0da8","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9041a9fd-4390-4f89-9a13-bfcfb40834f8","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4566a9fd-0daf-4770-82dd-0acf63d79b49","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef1a8598-0764-48a3-9999-f31a12127085","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire4/sFire4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sFire4","path":"sprites/sFire4/sFire4.yy",},
    "resourceVersion": "1.3",
    "name": "sFire4",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"327edc59-5bab-443c-84bc-55680646e89c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sFire4",
  "tags": [],
  "resourceType": "GMSprite",
}