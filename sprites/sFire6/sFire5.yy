{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 48,
  "bbox_top": 4,
  "bbox_bottom": 46,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 50,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"8007fe30-36b8-4f06-b9f0-29e26409b329","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"f2634179-6afe-4558-a0fe-59583a34fdb3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"6a587e9f-e662-4aab-892c-922bc389b654","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"e28e15ab-b525-4041-a15e-d98c75c5023b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"53248431-2c24-474f-8f20-761fe46a964c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"d6891f86-171e-427f-b6f5-c8691990576c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"88a122ea-e599-4658-8938-116cbc1cfa97","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"e4a16cdd-9954-49bc-880b-667847d9c687","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"086145eb-f538-4f8b-a21d-92153df4c08b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"37ffb947-8118-4406-a619-98857c1e3da1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"1197df5c-5b12-480b-9b53-4572faa75647","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"7a30241b-a29c-481c-b042-5cb97aa41e79","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire5/sFire5.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire5/sFire5.yy",},"LayerId":{"name":"327edc59-5bab-443c-84bc-55680646e89c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 25.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3d951309-09fe-4f78-a622-82b707875f50","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8007fe30-36b8-4f06-b9f0-29e26409b329","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c0980e3f-61f6-4873-b18b-ac799d5ce9e5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2634179-6afe-4558-a0fe-59583a34fdb3","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"916e1c30-3ca2-47fc-b310-e000cf9535fb","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"433bc79b-8ddc-4c3d-848a-31ec4825531b","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13c56305-18e9-4af7-a57f-b7abeffc3ef2","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9c0fcdb-cf94-4741-8be7-82fbe35b1326","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"276fbea1-6441-4f15-aa47-335425407119","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77287cc0-3c42-4bbd-901e-0d3a4018b409","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"145fa762-b73a-43d7-a910-a322b6d678a6","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f066f912-7aa5-4b7a-b220-e3298cd170b2","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3aa1c7f-aa65-4cca-ac65-3519fe6e3516","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6a587e9f-e662-4aab-892c-922bc389b654","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b623b451-c49b-40f2-b8f5-ec17267f37ba","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee7d40d1-82fd-4147-95ae-a544e3b1069e","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"96968e06-373e-4ad1-9148-fdf98c6961be","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e28e15ab-b525-4041-a15e-d98c75c5023b","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b81b03f-4d9b-44df-bfe2-314f76ca6ba0","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f4d96b3-8594-4740-9ad7-bfa0addb878a","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf02dd7e-91c0-4a80-b88a-ad0920c65ce3","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53248431-2c24-474f-8f20-761fe46a964c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e1d90e4f-97b9-4d2e-83c1-a0c37c7d56c7","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6891f86-171e-427f-b6f5-c8691990576c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e722e09c-699d-4555-9077-5210edfbe311","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"efa5306b-455b-4ac4-8240-ef8d04f55f3f","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c01d154f-9afb-4b6c-a8ea-8caa3ef8a173","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb0126fc-233a-47f7-b01d-fb014b26bb7c","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"30a7bc68-c5de-4141-9bdb-267c607563c3","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88a122ea-e599-4658-8938-116cbc1cfa97","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b567f80e-8edd-49de-aa40-369dc5be7132","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f21d251f-17e5-4e5d-8172-40aff48d18e9","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f5f71727-bac6-42b5-8ebd-2fc5d4359665","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e4a16cdd-9954-49bc-880b-667847d9c687","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e5e1e010-9e5e-48b4-81ad-332780d5022e","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"086145eb-f538-4f8b-a21d-92153df4c08b","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e348cd6-a9d0-4e7e-a4a4-d913e7579eaa","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b11c6c6-835e-4288-9c6c-55035fb1a2b9","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e097c065-d5ed-402e-a5de-b93b89037ae2","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c9613b1-0eb2-465d-9f32-10fca9baabe1","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eb97a7f2-4f26-44f4-baa4-c21f5de726ca","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"37ffb947-8118-4406-a619-98857c1e3da1","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1381518d-0196-46af-8a28-95003904c9ce","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1197df5c-5b12-480b-9b53-4572faa75647","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed9c534d-0a12-44a5-a5d4-ee4f7ce5d0c6","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a30241b-a29c-481c-b042-5cb97aa41e79","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6006676b-5aa8-4bbf-bbc8-5b2f6aa2f95e","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ed7a9fe-d354-4be5-b121-bacf5445f989","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e1c6169-09b2-4899-8859-795b550de805","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2508fe31-9c4b-4e6f-93ad-babfa61cc2a3","path":"sprites/sFire5/sFire5.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sFire5","path":"sprites/sFire5/sFire5.yy",},
    "resourceVersion": "1.3",
    "name": "sFire5",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"327edc59-5bab-443c-84bc-55680646e89c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sFire5",
  "tags": [],
  "resourceType": "GMSprite",
}