{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 15,
  "bbox_right": 48,
  "bbox_top": 32,
  "bbox_bottom": 53,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a0d3b84a-ad6d-4512-bfbe-42e4054686fb","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a0d3b84a-ad6d-4512-bfbe-42e4054686fb","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"a0d3b84a-ad6d-4512-bfbe-42e4054686fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2e93cb49-84eb-4a49-9b98-061350f7608e","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2e93cb49-84eb-4a49-9b98-061350f7608e","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"2e93cb49-84eb-4a49-9b98-061350f7608e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5503ecff-6e56-4f25-9f75-9955d4206975","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5503ecff-6e56-4f25-9f75-9955d4206975","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"5503ecff-6e56-4f25-9f75-9955d4206975","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"07f49193-4bc2-46cf-b800-88a0132d6f5c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"07f49193-4bc2-46cf-b800-88a0132d6f5c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"07f49193-4bc2-46cf-b800-88a0132d6f5c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5b1ea5eb-dfae-49aa-bdad-6700ee1b9154","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5b1ea5eb-dfae-49aa-bdad-6700ee1b9154","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"5b1ea5eb-dfae-49aa-bdad-6700ee1b9154","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3d9bfd9f-f9d4-45c0-936f-5bcbc8db17e8","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3d9bfd9f-f9d4-45c0-936f-5bcbc8db17e8","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"3d9bfd9f-f9d4-45c0-936f-5bcbc8db17e8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bed6b152-0fa8-4900-9516-c31c2b3b46e4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bed6b152-0fa8-4900-9516-c31c2b3b46e4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"bed6b152-0fa8-4900-9516-c31c2b3b46e4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8bc05314-4843-40b9-89e0-34cb60d0229c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8bc05314-4843-40b9-89e0-34cb60d0229c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"8bc05314-4843-40b9-89e0-34cb60d0229c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"72b65828-e1fb-42dc-8098-f9ab65ec6219","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72b65828-e1fb-42dc-8098-f9ab65ec6219","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"72b65828-e1fb-42dc-8098-f9ab65ec6219","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e5322500-9746-41e0-b723-7bfb4bf64cfc","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e5322500-9746-41e0-b723-7bfb4bf64cfc","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"e5322500-9746-41e0-b723-7bfb4bf64cfc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b023481-5f7d-4265-987d-b3bc8068c8a4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b023481-5f7d-4265-987d-b3bc8068c8a4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"2b023481-5f7d-4265-987d-b3bc8068c8a4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"50686716-3d5c-4d3e-961f-4df727b314f5","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"50686716-3d5c-4d3e-961f-4df727b314f5","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"50686716-3d5c-4d3e-961f-4df727b314f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1f95db05-d85b-4fdd-bf37-0357e3b14dcf","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1f95db05-d85b-4fdd-bf37-0357e3b14dcf","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"1f95db05-d85b-4fdd-bf37-0357e3b14dcf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ecdd7e0-41d7-4816-b88f-dba7bbc8cbf8","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ecdd7e0-41d7-4816-b88f-dba7bbc8cbf8","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"1ecdd7e0-41d7-4816-b88f-dba7bbc8cbf8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77117626-735d-4da7-b064-8cd02bcf3486","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77117626-735d-4da7-b064-8cd02bcf3486","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"77117626-735d-4da7-b064-8cd02bcf3486","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7257a1c2-fae6-42a8-b2ba-a82fb2b6ce85","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7257a1c2-fae6-42a8-b2ba-a82fb2b6ce85","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"7257a1c2-fae6-42a8-b2ba-a82fb2b6ce85","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2c1eac89-cf58-440b-a798-b0a05e12070f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2c1eac89-cf58-440b-a798-b0a05e12070f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"2c1eac89-cf58-440b-a798-b0a05e12070f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"64d61c73-f8f6-4fc3-b796-097bc76c58d6","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"64d61c73-f8f6-4fc3-b796-097bc76c58d6","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"64d61c73-f8f6-4fc3-b796-097bc76c58d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"80c59a78-6ba5-4b0d-b3b7-98967cced3ac","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"80c59a78-6ba5-4b0d-b3b7-98967cced3ac","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"80c59a78-6ba5-4b0d-b3b7-98967cced3ac","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b5a68e4a-c1a2-47fd-be30-20bca3e9499f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b5a68e4a-c1a2-47fd-be30-20bca3e9499f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"b5a68e4a-c1a2-47fd-be30-20bca3e9499f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"000d8de5-eb70-4395-9f12-182f1551aa8b","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"000d8de5-eb70-4395-9f12-182f1551aa8b","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"000d8de5-eb70-4395-9f12-182f1551aa8b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2cde43df-6453-4d98-83e2-8b9a905e757f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2cde43df-6453-4d98-83e2-8b9a905e757f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"2cde43df-6453-4d98-83e2-8b9a905e757f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65f6aefc-6876-4d7f-bb11-147bef4868c3","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65f6aefc-6876-4d7f-bb11-147bef4868c3","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"65f6aefc-6876-4d7f-bb11-147bef4868c3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"697236df-ec01-466d-bae4-8aba45af1613","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"697236df-ec01-466d-bae4-8aba45af1613","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"697236df-ec01-466d-bae4-8aba45af1613","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8d77fd1-31d4-4f30-b17d-e58324260946","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8d77fd1-31d4-4f30-b17d-e58324260946","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"a8d77fd1-31d4-4f30-b17d-e58324260946","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6b8d9465-a84f-4220-8eb6-ac1c9b7b4a13","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6b8d9465-a84f-4220-8eb6-ac1c9b7b4a13","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"6b8d9465-a84f-4220-8eb6-ac1c9b7b4a13","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b7f07d0f-981c-4cc0-92c8-143f42fea407","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b7f07d0f-981c-4cc0-92c8-143f42fea407","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"b7f07d0f-981c-4cc0-92c8-143f42fea407","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77d405c6-dc3e-402e-845e-758c151a8970","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77d405c6-dc3e-402e-845e-758c151a8970","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"77d405c6-dc3e-402e-845e-758c151a8970","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6dc6206f-cd1d-4d78-bcfc-7d9ecaa81f8c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6dc6206f-cd1d-4d78-bcfc-7d9ecaa81f8c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"6dc6206f-cd1d-4d78-bcfc-7d9ecaa81f8c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b14f132a-a64f-4ccd-8a31-64eaf69f03bb","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b14f132a-a64f-4ccd-8a31-64eaf69f03bb","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"b14f132a-a64f-4ccd-8a31-64eaf69f03bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc7e5b9e-5856-489c-bda5-c7b0c50ebc82","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc7e5b9e-5856-489c-bda5-c7b0c50ebc82","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"cc7e5b9e-5856-489c-bda5-c7b0c50ebc82","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"786c140c-6d02-4572-b9c8-4454ca034161","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"786c140c-6d02-4572-b9c8-4454ca034161","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"786c140c-6d02-4572-b9c8-4454ca034161","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05101883-b58e-419a-8fde-6139abd20072","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05101883-b58e-419a-8fde-6139abd20072","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"05101883-b58e-419a-8fde-6139abd20072","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f42f1d5f-cdcc-42e0-8933-95edab9d2acf","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f42f1d5f-cdcc-42e0-8933-95edab9d2acf","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"f42f1d5f-cdcc-42e0-8933-95edab9d2acf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fa3289ec-516f-4766-9cae-62257ad3f8f4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fa3289ec-516f-4766-9cae-62257ad3f8f4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"fa3289ec-516f-4766-9cae-62257ad3f8f4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"57bcab53-e712-489a-a0e0-7c8186536403","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"57bcab53-e712-489a-a0e0-7c8186536403","path":"sprites/sGoldArmor/sGoldArmor.yy",},"LayerId":{"name":"991d8d77-9971-414e-8f5f-eeffecfae205","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","name":"57bcab53-e712-489a-a0e0-7c8186536403","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 36.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cf2c9783-c5ad-4aa9-92e4-191f7bd9cf6b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a0d3b84a-ad6d-4512-bfbe-42e4054686fb","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c04ce48-3a9e-4a0f-8ace-01467b83d73d","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2e93cb49-84eb-4a49-9b98-061350f7608e","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eb5bb762-a06d-499f-bd24-7d3e4f6b1961","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5503ecff-6e56-4f25-9f75-9955d4206975","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f26f22f5-f05c-4d46-a12e-896b7f47f02d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"07f49193-4bc2-46cf-b800-88a0132d6f5c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0e6a4e95-144a-4881-89ab-b707053c0116","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5b1ea5eb-dfae-49aa-bdad-6700ee1b9154","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5f7821cb-5900-4fdb-b0a9-fee815ddd06c","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3d9bfd9f-f9d4-45c0-936f-5bcbc8db17e8","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22735e57-7316-4e55-85d0-e2ccee261d2e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bed6b152-0fa8-4900-9516-c31c2b3b46e4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"51a6853a-22b0-4b14-a3af-43e865a07056","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8bc05314-4843-40b9-89e0-34cb60d0229c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"de957334-b56e-4aa8-b754-bf5ea313320b","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72b65828-e1fb-42dc-8098-f9ab65ec6219","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"742dfdd6-1af2-4d01-b9f1-70e044022c66","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e5322500-9746-41e0-b723-7bfb4bf64cfc","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4aa1f739-a30b-434d-a2f8-8e5b5b1751ef","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b023481-5f7d-4265-987d-b3bc8068c8a4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"507f0b22-c909-479a-8d4b-2141c4a72283","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"50686716-3d5c-4d3e-961f-4df727b314f5","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5dfc0f16-0878-48f4-a847-b6d2b4ea1418","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1f95db05-d85b-4fdd-bf37-0357e3b14dcf","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4a992446-fb8d-491d-add9-e97ad41698d0","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ecdd7e0-41d7-4816-b88f-dba7bbc8cbf8","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1bb2720d-6d7b-4a0a-aeb4-7d8c489c5eab","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77117626-735d-4da7-b064-8cd02bcf3486","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af8ce471-6589-426c-a182-28a37d1421cf","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7257a1c2-fae6-42a8-b2ba-a82fb2b6ce85","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"800dcb0c-3f1b-42b4-8c81-7ee21f6919cf","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2c1eac89-cf58-440b-a798-b0a05e12070f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8745421e-452f-4193-9e0d-09a81e287923","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"64d61c73-f8f6-4fc3-b796-097bc76c58d6","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f266d329-a662-44d5-8148-1fa216fd6ce2","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"80c59a78-6ba5-4b0d-b3b7-98967cced3ac","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c0b256f-e13e-4e52-b110-e43dd0a19a01","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b5a68e4a-c1a2-47fd-be30-20bca3e9499f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"93f89af4-d4ba-44df-9942-37bc1414b4bd","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"000d8de5-eb70-4395-9f12-182f1551aa8b","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5fb697bb-ac1c-4cb5-815e-43d24888c77f","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2cde43df-6453-4d98-83e2-8b9a905e757f","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"37a73d7f-7ca9-49eb-900a-ec5696b04625","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65f6aefc-6876-4d7f-bb11-147bef4868c3","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f07e8c3c-f3f1-4a4e-832e-e48beb119347","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"697236df-ec01-466d-bae4-8aba45af1613","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"314fa8ce-71cd-4e17-91a2-60294e5e6d83","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8d77fd1-31d4-4f30-b17d-e58324260946","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2426eb9b-d331-4ce7-89a4-751a055b4a37","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6b8d9465-a84f-4220-8eb6-ac1c9b7b4a13","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cdff2e79-6c63-4710-bc45-7310e76671a9","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b7f07d0f-981c-4cc0-92c8-143f42fea407","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"457cde19-a772-4332-9414-8de6dd79e938","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77d405c6-dc3e-402e-845e-758c151a8970","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aa04086a-3467-4d39-8d39-a25f2e60698d","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dc6206f-cd1d-4d78-bcfc-7d9ecaa81f8c","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cd488d6f-9f18-4175-a9a2-210b190edec2","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b14f132a-a64f-4ccd-8a31-64eaf69f03bb","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"073cea2f-e427-482f-a46c-5c3d21dc82a1","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc7e5b9e-5856-489c-bda5-c7b0c50ebc82","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9ba0b0c5-1cf2-4b8c-ae5b-cd1be38748fa","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"786c140c-6d02-4572-b9c8-4454ca034161","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"15cd5264-6c79-4057-9aed-5b98d143c357","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05101883-b58e-419a-8fde-6139abd20072","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f920178e-d00c-4e60-bde5-d16661f2e86a","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f42f1d5f-cdcc-42e0-8933-95edab9d2acf","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8b076d01-6aec-41c5-9851-9388f606938e","Key":34.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fa3289ec-516f-4766-9cae-62257ad3f8f4","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1bb36d8e-12bb-4cd2-b183-ece2388211f8","Key":35.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"57bcab53-e712-489a-a0e0-7c8186536403","path":"sprites/sGoldArmor/sGoldArmor.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sGoldArmor","path":"sprites/sGoldArmor/sGoldArmor.yy",},
    "resourceVersion": "1.3",
    "name": "sGoldArmor",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"991d8d77-9971-414e-8f5f-eeffecfae205","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Equipment",
    "path": "folders/Sprites/Player Sprites/Equipment.yy",
  },
  "resourceVersion": "1.0",
  "name": "sGoldArmor",
  "tags": [],
  "resourceType": "GMSprite",
}