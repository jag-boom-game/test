c_tan = make_color_rgb(210,180,140);
c_brown = make_color_rgb(210,105,30);

draw_rectangle_color(0,0,room_width,room_height,c_tan,c_brown,c_tan,c_brown,false);
draw_rectangle_color(0,0,room_width,room_height,c_white,c_white,c_white,c_white,true);
		
draw_set_font(fnt_Default);
draw_text_transformed(15,5,"Player Status",0.50,0.5,0);

for (var i = 0; i < 5; i++) {
	draw_text_transformed(30,30+30*i,ds_map_find_value(stats,i),0.5,0.5,0);	
}