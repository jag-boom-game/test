display_set_gui_size(1366,768);

middle_h = display_get_gui_height()/2;
middle_w = display_get_gui_width()/2;


//Stats to Be Displayed
stats = ds_map_create();
ds_map_add(stats,0,"Level: " + string(global.level));
ds_map_add(stats,1,"XP: " + string(global.xp) + "/" + string(global.maxxp));
ds_map_add(stats,2,"HP: " + string(global.hp) + "/" + string(global.maxhp));
ds_map_add(stats,3,"Mana: " + string(global.mana) + "/" + string(global.maxmana));
ds_map_add(stats,4,"ATT: " + string(global.ATT));