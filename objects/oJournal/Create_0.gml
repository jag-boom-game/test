/// @description Insert description here
// You can write your code in this editor
display_set_gui_size(1366,768);

middle_h = display_get_gui_height()/2;
middle_w = display_get_gui_width()/2;

journal_entries = ds_map_create();

ds_map_add(journal_entries,0,"I listened to the old man's story in the Village.");
ds_map_add(journal_entries,1,"I entered the cave, and all I found were some slimes...");