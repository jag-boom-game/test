c_tan = make_color_rgb(210,180,140);
c_brown = make_color_rgb(210,105,30);

draw_rectangle_color(0,0,room_width,room_height,c_tan,c_brown,c_tan,c_brown,false);
draw_rectangle_color(0,0,room_width,room_height,c_white,c_white,c_white,c_white,true);
		
draw_set_font(fnt_Default);
draw_text_transformed(15,5,"Journal",0.50,0.5,0);

for (var i = 0; i <= ds_map_size(journal_entries)-1; i++) {
	if (ds_map_find_value(oGame.journal_entries_locked,i) == true) {

		draw_text_transformed(15,15*(i+1),ds_map_find_value(journal_entries,i),0.25,0.25,0);
	}
}