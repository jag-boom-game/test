keyNames = ds_list_create();
ds_list_add(keyNames,
"Left: ",
"Right: ",
"Up: ",
"Down: ",
"Activate: ",
"Attack: ",
"Magic: ",
"Item: ",
"Back");

display_set_gui_size(room_width,room_height);

middle_h = display_get_gui_height()/2;
middle_w = display_get_gui_width()/2;

menu = 0;
menu_option = 0;

draw_set_font(fnt_Default);
font_scale = 1;

keysCombined = ds_list_create();

pending = false;