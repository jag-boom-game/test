draw_set_font(fnt_Default);
/*
for (var i = 0; i < ds_list_size(oGame.keys); i++) {
	draw_text(10,75*i,ds_list_find_value(keyNames,i) + ds_list_find_value(oGame.keys,i));
}
*/
draw_text(10,10,ds_list_find_value(oGame.keys,menu_option));
draw_text(10,30,ds_map_find_value(oGame.keyBindings,menu_option));
draw_text(10,50,ds_list_find_value(keysCombined,menu_option));

for (var i = 0; i <= ds_list_size(keyNames)-1; i++) {
	if (menu_option == i) {
		if (pending == false) {
			draw_set_alpha(Wave(0.25,1,1,0));
		} else {draw_set_alpha(1);}
	draw_set_color(c_blue);
	draw_text(middle_w-string_width(string(ds_list_find_value(keysCombined,menu_option)))/2, 
	middle_h+string_height(string(ds_list_find_value(keysCombined,menu_option)))*2*i,
	ds_list_find_value(keysCombined,menu_option));
	draw_set_color(c_white);
	draw_set_alpha(1);
		}
	else {
		draw_set_color(c_black);
		draw_set_alpha(1);
		draw_text((middle_w-string_width(string(ds_list_find_value(keysCombined,i)))/2) + 4, 
		(middle_h+string_height(string(ds_list_find_value(keysCombined,i)))*2*i) + 2,
		ds_list_find_value(keysCombined,i));
		draw_set_color(c_white);
		draw_text(middle_w-string_width(string(ds_list_find_value(keysCombined,i)))/2, 
		middle_h+string_height(string(ds_list_find_value(keysCombined,i)))*2*i,
		ds_list_find_value(keysCombined,i));
	}
}

draw_set_font(fnt_Title);