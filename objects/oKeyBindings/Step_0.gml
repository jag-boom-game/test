for (var i = 0; i < ds_list_size(keyNames); i++) {
	ds_list_add(keysCombined,string(ds_list_find_value(keyNames,i)+string(ds_list_find_value(oGame.keys,i))))
}

if (pending == false) {
	if (global.keydown) {menu_option++;}
	if (global.keyup) {menu_option--;}
} else {}
if (keyboard_check_pressed(vk_escape)) {game_end();}


if (menu_option < 0) {menu_option = ds_list_size(keyNames)-1;}
if (menu_option > ds_list_size(keyNames)-1) {menu_option = 0;}

if (global.keyCommitted) {
	if (pending == false) {pending = true;} else {pending = false;}
	keyboard_string = "";
			if (menu_option == 8) { //Back
				instance_create_depth(x,y,depth,oTitle);
				oTitle.menu = 2;
				oTitle.menu_option = 2;
				instance_destroy();
			}	
}

switch (menu_option) {
		
	case 0: //Left
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("A"));
				//keyboard_set_map(ord("A"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}
	break;
			
	case 1: //Right
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				if (keyboard_lastkey <= 122) && (keyboard_lastkey >= 65) {
					for (var i = 0; i <= ds_map_size(oGame.keyBindings); i++) {
						if (keyboard_lastkey != ds_map_find_value(oGame.keyBindings,i)) {
					
							keyboard_set_map(keyboard_lastkey,ord("D"));
							ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
							ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
							ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
							pending = false;
							} else {pending = false;}
					}
				}
			}
		}				
	break; 
			
	case 2: //Up
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("W"));
				//keyboard_set_map(ord("W"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}					
	break;
			
	case 3: //Down
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("S"));
				//keyboard_set_map(ord("S"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}					
	break;
			
	case 4: //Activate
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("K"));
				//keyboard_set_map(ord("K"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}					
	break;
			
	case 5: //Attack
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("L"));
				//keyboard_set_map(ord("L"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}					
	break;
			
	case 6: //Magic
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("J"));
				//keyboard_set_map(ord("J"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}					
	break;
			
	case 7: //Item
		if (keyboard_check_pressed(vk_anykey)) {	
			if (pending == true) {
				keyboard_set_map(keyboard_lastkey,ord("I"));
				//keyboard_set_map(ord("I"),vk_nokey);
				ds_list_replace(oGame.keys,menu_option,string_upper(keyboard_string));
				ds_map_replace(oGame.keyBindings,menu_option,ord(keyboard_string));
				ds_list_replace(keysCombined,menu_option,string(ds_list_find_value(keyNames,menu_option)+string(ds_list_find_value(oGame.keys,menu_option))));
				pending = false;} 
			else {pending = false;}
		}					
	break;
			
}

if (keyboard_check_pressed(vk_numpad5)) {
	instance_create_depth(x,y,depth,oTitle);
	if (instance_exists(oTitle)) {	
		oTitle.menu = 2;
		oTitle.menu_option = 2;
	}
		instance_destroy();
}