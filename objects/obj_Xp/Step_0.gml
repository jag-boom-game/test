/// Magnetize to player

if (instance_exists(oPlayer)) {
	var dis = point_distance(x,y,oPlayer.x,oPlayer.y)
	if (dis < sight) {
		targetx = oPlayer.x;
		targety = oPlayer.y;
		x += sign(targetx - x)*spd;
		y += sign(targety - y)*spd;
	}
}