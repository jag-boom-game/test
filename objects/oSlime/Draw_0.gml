/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if (variation = 1) {
draw_sprite_ext(
	sprite_index,
	image_index,
	floor(x),
	floor(y-z),
	image_xscale,
	image_yscale,
	image_angle,
	c_orange,
	image_alpha);
}

if (variation = 2) {
draw_sprite_ext(
	sprite_index,
	image_index,
	floor(x),
	floor(y-z),
	image_xscale,
	image_yscale,
	image_angle,
	c_red,
	image_alpha);
}