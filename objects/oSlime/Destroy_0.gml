/// @desc Drop items and fragments
if  (entityFragmentCount > 0)
{
	fragmentArray = array_create(entityFragmentCount, entityFragment)
	repeat (1 + variation) DropItems(x,y,fragmentArray)
}
