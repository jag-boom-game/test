draw_set_font(fnt_Default)
for (var i = 0; i <= ds_grid_height(title_options) -1; i++) {
		if (menu_option == i) {
		draw_set_alpha(Wave(0.25,1,1,0));
		draw_set_color(c_blue);
		draw_text(middle_w-string_width(string(ds_grid_get(title_options,menu,menu_option)))/2, 
	middle_h+string_height(string(ds_grid_get(title_options,menu,menu_option)))*2*i,
	ds_grid_get(title_options,menu,menu_option));
	draw_set_color(c_white);
	draw_set_alpha(1);
		}
	else {
		draw_set_color(c_black);
		draw_set_alpha(1);
		draw_text((middle_w-string_width(string(ds_grid_get(title_options,menu,i)))/2) + 4, 
		(middle_h+string_height(string(ds_grid_get(title_options,menu,i)))*2*i) + 2,
		ds_grid_get(title_options,menu,i));
		draw_set_color(c_white);
		draw_text(middle_w-string_width(string(ds_grid_get(title_options,menu,i)))/2, 
		middle_h+string_height(string(ds_grid_get(title_options,menu,i)))*2*i,
		ds_grid_get(title_options,menu,i));
	}
}

/*
draw_text(10,10,"Menu: " + string(menu));
draw_text(10,25,"Menu Option: " + string(menu_option));
draw_text(10,40,"i: " + string(ds_grid_get(title_options,menu,menu_option)));
*/
draw_set_font(fnt_Title);

if (menu == 0) {
draw_text_transformed_color((middle_w-string_width(string("Carpe Noctum"))/2),Wave(0,room_height-string_height("Carpe Noctum"),5,0),"Carpe Noctum",font_scale,font_scale,0,c_white,c_white,c_white,c_white,Wave(1,0.1,3,0));
draw_text_transformed_color((middle_w-string_width(string("Carpe Noctum"))/2),Wave(room_height-string_height("Carpe Noctum"),0,5,0),"Carpe Noctum",font_scale,font_scale,0,c_black,c_black,c_black,c_black,Wave(1,0.1,4,0));
//draw_text_transformed_color((middle_w-string_width(string("Carpe Noctum"))/2),(string_height(string("Carpe Noctum"))),"Carpe Noctum",1,1,Wave(90,270,5,0),c_white,c_black,c_white,c_black,1);
//draw_text_transformed_color((middle_w-string_width(string("Carpe Noctum"))/2),(string_height(string("Carpe Noctum"))),"Carpe Noctum",1,1,Wave(270,90,5,0),c_white,c_black,c_white,c_black,1);
}
draw_text_transformed_color(middle_w-string_width(string("Carpe Noctum"))/2,middle_h-string_height(string("Carpe Noctum")),"Carpe Noctum",font_scale,font_scale,0,c_black,c_white,c_black,c_white,1);