//if (keyboard_check_pressed(ord("A"))) {menu--;}
//if (keyboard_check_pressed(ord("D"))) {menu++;}
if (global.keydown) {menu_option++;}
if (global.keyup) {menu_option--;}
if (keyboard_check_pressed(vk_escape)) {game_end();}


if (menu_option < 0) {menu_option = ds_grid_height(title_options) -1}
if (menu_option > ds_grid_height(title_options) -1) {menu_option = 0;}

if (global.keyCommitted) {
	
	switch (menu) {
	
		case 0: //Main Menu Options
			switch (menu_option) {
		
				case 0: //New Game
				room_goto(rInit);
				break;
			
				case 1: //Continue
				room_goto(rInit);
				break; 
			
				case 2: //Load Game
				menu++;
				menu_option = 0;
				break;
			
				case 3: //Options
				menu+=2;
				menu_option = 0;
				break;
			
				case 4: //Quit
				game_end();
				break;
			
				default:
				break;
			}
		break;
	
		case 1: //Load Save File Menu Options
			if (instance_exists(oGame)) {
				instance_destroy(oGame);
				instance_create_depth(x,y,depth,oGame);
			} 
			switch (menu_option) {

			
				case 0: //Save Game 1
					if (!instance_exists(oGame)) {instance_create_depth(x,y,depth,oGame);}
					if (file_exists("savedgame1.sav")) {
						var _wrapper = LoadJSONFromFile("savedgame1.sav");
						var _list = _wrapper[? "ROOT"];
					
						for (var i = 0; i < ds_list_size(_list); i++) {
							var _map = _list[| i];
							var _obj = _map[? "obj"];
							with (instance_create_layer(0,0,layer,oPlayer)) {
								x = _map[? "x"];
								y = _map[? "y"];
								image_blend = _map[? "image_blend"];
								image_index = _map[? "image_index"];
								room = _map[? "room"];
							}
						}
					
						ds_map_destroy(_wrapper);
						show_debug_message("Game Loaded");

					}
				break;
			
				case 1: //Save Game 2
					if (!instance_exists(oGame)) {instance_create_depth(x,y,depth,oGame);}
					if (file_exists("savedgame2.sav")) {
						var _wrapper = LoadJSONFromFile("savedgame2.sav");
						var _list = _wrapper[? "ROOT"];
					
						for (var i = 0; i < ds_list_size(_list); i++) {
							var _map = _list[| i];
							var _obj = _map[? "obj"];
							with (instance_create_layer(0,0,layer,oPlayer)) {
								x = _map[? "x"];
								y = _map[? "y"];
								image_blend = _map[? "image_blend"];
								image_index = _map[? "image_index"];
								room = _map[? "room"];
							}
						}
					
						ds_map_destroy(_wrapper);
						show_debug_message("Game Loaded");

					}
				break; 
			
				case 2: //Save Game 3
					if (!instance_exists(oGame)) {instance_create_depth(x,y,depth,oGame);}
					if (file_exists("savedgame3.sav")) {
						var _wrapper = LoadJSONFromFile("savedgame3.sav");
						var _list = _wrapper[? "ROOT"];
					
						for (var i = 0; i < ds_list_size(_list); i++) {
							var _map = _list[| i];
							var _obj = _map[? "obj"];
							with (instance_create_layer(0,0,layer,oPlayer)) {
								x = _map[? "x"];
								y = _map[? "y"];
								image_blend = _map[? "image_blend"];
								image_index = _map[? "image_index"];
								room = _map[? "room"];
							}
						}
					
						ds_map_destroy(_wrapper);
						show_debug_message("Game Loaded");

					}
				break;
			
				case 3: //Save Game 4
					if (!instance_exists(oGame)) {instance_create_depth(x,y,depth,oGame);}
					if (file_exists("savedgame4.sav")) {
						var _wrapper = LoadJSONFromFile("savedgame4.sav");
						var _list = _wrapper[? "ROOT"];
					
						for (var i = 0; i < ds_list_size(_list); i++) {
							var _map = _list[| i];
							var _obj = _map[? "obj"];
							with (instance_create_layer(0,0,layer,oPlayer)) {
								x = _map[? "x"];
								y = _map[? "y"];
								image_blend = _map[? "image_blend"];
								image_index = _map[? "image_index"];
								room = _map[? "room"];
							}
						}
					
						ds_map_destroy(_wrapper);
						show_debug_message("Game Loaded");

					}
				break;
			
				case 4: //Back
				menu--;
				menu_option = 0;
				break;
			
				default:
				break;
			}
		break;
	
		case 2: //Options Menu Options
			switch (menu_option) {
		
				case 0: //Volume
				break;
			
				case 1: //Brightness
				break; 
			
				case 2: //Key Bindings
					global.keyCommitted = false;
					instance_create_depth(x,y,depth,oKeyBindings);
					instance_destroy();
				break;
			
				case 3: //Roll Credits
					instance_create_depth(x,y,depth,oCredits);
					instance_create_layer(0,room_height+oCredits.h+(240*array_length(oCredits.role)),layer,oCreditsReset)
					instance_destroy();
					menu_option = 0;
				break;
			
				case 4: //Back
					menu-=2;
					menu_option = 0;
				break;
			
				default:
				break;
			}
		break;
	
	default:
	break;
	
	}

}