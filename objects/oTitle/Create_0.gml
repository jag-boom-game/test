title_options = ds_grid_create(5,5) 

ds_grid_add(title_options, 0,0,"New Game")
ds_grid_add(title_options,0,1,"Continue")
ds_grid_add(title_options,0,2,"Load")
ds_grid_add(title_options,0,3,"Options")
ds_grid_add(title_options,0,4,"Quit")


ds_grid_add(title_options,1,0,"File 1")
ds_grid_add(title_options,1,1,"File 2")
ds_grid_add(title_options,1,2,"File 3")
ds_grid_add(title_options,1,3,"File 4")
ds_grid_add(title_options,1,4,"Back")


ds_grid_add(title_options,2,0,"Volume")
ds_grid_add(title_options,2,1,"Brightness")
ds_grid_add(title_options,2,2,"Key Bindings")
ds_grid_add(title_options,2,3,"Credits")
ds_grid_add(title_options,2,4,"Back")

display_set_gui_size(room_width,room_height);

middle_h = display_get_gui_height()/2;
middle_w = display_get_gui_width()/2;

menu = 0;
menu_option = 0;

draw_set_font(fnt_Default);
font_scale = 1;