/// Cause a room transition
/// place "room_goto(targetRoom)" after oPlayer.direction on next line to SNAP transitions

if (instance_exists(oPlayer)) && (position_meeting(oPlayer.x,oPlayer.y,id))
{
	global.targetRoom = targetRoom;
	global.targetX = targetX;
	global.targetY = targetY;
	global.targetDirection = oPlayer.direction;
	with (oPlayer) state = PlayerStateTransition;
	RoomTransition(TRANS_TYPE.SLIDE, targetRoom)
	if (global.targetRoom = rCave1) {
		if (oGame.journal_entries_locked[? 1] == false) {
			ds_map_set(oGame.journal_entries_locked,1,true);
			NewJournal();
		}
	}
	instance_destroy();
}
