/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if (!global.gamePaused) {
	if (instance_exists(portal)) {
		image_speed = 0;
	} else {image_speed = 0.25;}

	if (image_index <= 0) {
		portal = instance_create_depth(x+sprite_width*1.5,y+sprite_height/2,depth,oSummonDemon);
		portal.creator = id;
	}
} else {image_speed = 0;}