draw_set_alpha(fadein);
fadein += 0.005;
draw_rectangle_color(0,0,room_width,room_height,c_black,c_black,c_black,c_black,false);

draw_set_alpha(1);
draw_set_color(c_white);
draw_text(display_get_gui_width()/2-string_width("Game Over!")/2,display_get_gui_height()/2-string_height("Game Over!")/2,"Game Over!");