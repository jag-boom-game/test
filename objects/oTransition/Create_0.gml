/// @desc Enums + Setup

enum TRANS_TYPE
{
	SLIDE,
	FADE,
	PUSH,
	STAR,
	WIPE
}


width = RESOLUTION_W
height = RESOLUTION_H;
heightHalf = (height * 0.5)+10;
percent = 0;
leading = OUT;
