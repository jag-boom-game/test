/// @desc Progress Transition

	with (oPlayer) state = PlayerStateTransition;

	if (leading == OUT)
	{
		percent = min(1, percent + TRANSITION_SPEED)
		if (percent >= .9999) {	
			oPlayer.x = global.targetX;
			oPlayer.y = global.targetY;
		}
		
		if (percent >= 1) //if screen fully obscured
		{
			room_goto(target);
			leading = IN
		}
	}
	else //leading == IN
	{
		percent = max(0, percent - TRANSITION_SPEED)

		if (percent <=0) //if screen fully in view
		{
			with (oPlayer) state = PlayerStateFree();
			instance_destroy();
		}
	}
