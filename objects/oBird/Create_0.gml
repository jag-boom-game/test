// Inherit the parent event
event_inherited();

state = ENEMYSTATE.WANDER

//Enemy Sprites
sprMove = sBird
sprIdle = sBirdIdle
sprEat = sBirdEat


//Enemy Scripts
enemyScript[ENEMYSTATE.WANDER] = BirdWander

original_scale = 1;

image_xscale *= original_scale;
image_yscale *= original_scale;