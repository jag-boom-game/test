if (keyboard_check_pressed(ord("S"))) {menu_option++;}
if (keyboard_check_pressed(ord("W"))) {menu_option--;}
if (keyboard_check_pressed(vk_escape)) {game_end();}

key_committed = (keyboard_check_pressed(vk_enter));


if (menu_option < 0) {menu_option = ds_list_size(menu_options)-1}
if (menu_option > ds_list_size(menu_options)-1) {menu_option = 0;}

if (key_committed) {
	
		switch (menu_option) {
	
			case 0: //Inventory

			break;
			
			case 1: //Status
			instance_create_depth(x,y,depth,oStatus);
			instance_destroy();
			break; 
			
			case 2: //Options

			break;
			
			case 3: //Journal
			instance_create_depth(x,y,depth,oJournal);
			instance_destroy();
			break;
			
			case 4: //Quit	
			if show_question("Are you sure you want to quit without saving?") {
				game_end();		
			} else {}
			break;
			
			case 5: //Back
			global.gamePaused = false;
			instance_destroy();
			break;
			
			default:
			global.gamePaused = false;
			instance_destroy();
			break;
		}
}

if (keyboard_check_pressed(vk_backspace)) {
	global.gamePaused = false;
	instance_destroy();
}