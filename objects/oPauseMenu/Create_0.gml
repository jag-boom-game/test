if (oPlayer.new_journal) {oPlayer.new_journal = false;}

display_set_gui_size(1366,768);

middle_h = display_get_gui_height()/2;
middle_w = display_get_gui_width()/2;

draw_set_font(fnt_Default);
draw_set_halign(fa_left);

menu_option = 0;

menu_options = ds_list_create();
ds_list_add(menu_options,"Inventory");
ds_list_add(menu_options,"Status");
ds_list_add(menu_options,"Options");
ds_list_add(menu_options,"Journal");
ds_list_add(menu_options,"Quit");
ds_list_add(menu_options,"Back");

team_options = ds_list_create();

status_options = ds_list_create();

options_options = ds_list_create();
	ds_list_add(options_options,"Audio");
		audio_options = ds_list_create();
			ds_list_add(audio_options,"Master Volume");
			ds_list_add(audio_options,"SFX Volume");
			ds_list_add(audio_options,"Music Volume");
			ds_list_add(audio_options,"Back");
	ds_list_add(options_options,"Video");
		video_options = ds_list_create();
			ds_list_add(video_options,"Brightness");
			ds_list_add(video_options,"Full Screen On/Off");
			ds_list_add(video_options,"Resolution");
			ds_list_add(video_options,"Back");
	ds_list_add(options_options,"Game");
		game_options = ds_list_create();
			ds_list_add(game_options,"Controls");
			ds_list_add(game_options,"Text Speed");
			ds_list_add(game_options,"Back");
	ds_list_add(options_options,"Back");
journal_options = ds_list_create();

