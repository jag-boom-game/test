draw_rectangle_color(middle_w-string_width(ds_list_find_value(files,1)),
middle_h-string_height(string(ds_list_find_value(files,1))),
middle_w+string_width(ds_list_find_value(files,1)),
middle_h+string_height(string(ds_list_find_value(files,1)))*ds_list_size(files)*2,
c_black,
c_gray,
c_black,
c_gray,
false);

draw_rectangle_color(middle_w-string_width(ds_list_find_value(files,1)),
middle_h-string_height(string(ds_list_find_value(files,1))),
middle_w+string_width(ds_list_find_value(files,1)),
middle_h+string_height(string(ds_list_find_value(files,1)))*ds_list_size(files)*2,
c_white,
c_white,
c_white,
c_white,
true);


for (var i = 0; i <= ds_list_size(files)-1; i++) {
	if (menu_option == i) {
		draw_set_alpha(Wave(0.5,1,1,0));
		draw_set_color(c_red);
		draw_text(middle_w-string_width(string(ds_list_find_value(files,menu_option)))/2, 
			middle_h+string_height(string(ds_list_find_value(files,menu_option)))*2*i,
			ds_list_find_value(files,menu_option));
		draw_set_color(c_white);
		draw_set_alpha(1);
		}
	else {
		draw_set_color(c_black);
		draw_set_alpha(1);
		draw_text((middle_w-string_width(string(ds_list_find_value(files,i)))/2) + 4, 
		(middle_h+string_height(string(ds_list_find_value(files,i)))*2*i) + 2,
		ds_list_find_value(files,i));
		draw_set_color(c_white);
		draw_text(middle_w-string_width(string(ds_list_find_value(files,i)))/2, 
		middle_h+string_height(string(ds_list_find_value(files,i)))*2*i,
		ds_list_find_value(files,i));
	}
}