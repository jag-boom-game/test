if (keyboard_check_pressed(ord("S"))) {menu_option++;}
if (keyboard_check_pressed(ord("W"))) {menu_option--;}
if (keyboard_check_pressed(vk_escape)) {game_end();}
key_committed = (keyboard_check_pressed(vk_enter));

if (menu_option < 0) {menu_option = ds_list_size(files) - 1}
if (menu_option > ds_list_size(files) -1) {menu_option = 0;}

if (key_committed) {

		switch (menu_option) {
		
			case 0:
			global.gamePaused = false;
			instance_destroy();
			break;
			
			case 1:
			SaveGame();
			show_message("File 1 Saved!")
			break; 
			
			case 2:
			SaveGame();
			show_message("File 2 Saved!")
			break;
			
			case 3:
			SaveGame();
			show_message("File 3 Saved!")
			break;
			
			case 4:
			SaveGame();
			show_message("File 4 Saved!")
			break;
			
			default:
			break;
		}
}