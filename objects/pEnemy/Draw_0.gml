/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if (justhit) {
	//Draw Health Bar
	if (enemyHP > 0) {	
		if (enemyHP <= enemymaxhp) && (enemyHP >= enemymaxhp*2/3) {
			draw_set_color(c_lime);
		}
		
		if (enemyHP < enemymaxhp*2/3) && (enemyHP >= enemymaxhp/3) {
			draw_set_color(c_yellow);
		}
		
		if (enemyHP < enemymaxhp/3) && (enemyHP > 0) {
			draw_set_color(c_red);
		} 
	
	draw_rectangle(x-sprite_width,
	y-sprite_height,
	(x-sprite_width)+(((x+sprite_width-(x-sprite_width))*enemyHP)/enemymaxhp),
	y-sprite_height-sprite_width/4,
	false);
	
	//Health Bar Outline
	draw_set_color(c_white);
	draw_rectangle(x-sprite_width,
	y-sprite_height,
	x+sprite_width,
	y-sprite_height-sprite_width/4,
	true);
	}
}