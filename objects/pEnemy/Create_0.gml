event_inherited()

//Intrinsic Variables
state = ENEMYSTATE.IDLE
hspd = 0
vspd = 0
xTo = xstart
yTo = ystart
dir = 0
aggroCheck = 0
aggroCheckDuration = 5
stateTarget = state
statePrevious = state
stateWait = 0
stateWaitDuration = 0

// Enemy Stats
timePassed = 0
waitDuration = random_range(40,100)
wait = 0

//Enemy Sprites
sprMove = sSlime

//Enemy Scripts
enemyScript[ENEMYSTATE.IDLE] = -1
enemyScript[ENEMYSTATE.WANDER] = -1
enemyScript[ENEMYSTATE.CHASE] = -1
enemyScript[ENEMYSTATE.ATTACK] = -1
enemyScript[ENEMYSTATE.HURT] = -1
enemyScript[ENEMYSTATE.DIE] = -1
enemyScript[ENEMYSTATE.WAIT] = EnemyWait

enemymaxhp = enemyHP;
justhit = false;