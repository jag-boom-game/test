// Play Music in this room
if (room = rTitle) {
	if (!audio_is_playing(sn_Title)){
		audio_play_sound(sn_Title,1000,true);}
} else {
if (audio_is_playing(sn_Title)) {
	audio_stop_sound(sn_Title);}
} 

if (room = rVillage) {
	if (!audio_is_playing(mDali)){
		audio_play_sound(mDali,1000,true);}
} else {
if (audio_is_playing(mDali)) {
audio_stop_sound(mDali);}
} 

if (room = rNewVillage) {
	if (!audio_is_playing(mNewVillage)){
		audio_play_sound(mNewVillage,1000,true);}
} else {
if (audio_is_playing(mNewVillage)) {
audio_stop_sound(mNewVillage);}
} 

if (room = rRiver) {
	if (!audio_is_playing(mFarHorizons)){
		audio_play_sound(mFarHorizons,1000,true);}
} else {
if (audio_is_playing(mFarHorizons)) {
	audio_stop_sound(mFarHorizons);}
} 

if (room = rCave1) {
	if (!audio_is_playing(mCave1)){
		audio_play_sound(mCave1,1000,true);}
} else {
if (audio_is_playing(mCave1)) {
	audio_stop_sound(mCave1);}
} 