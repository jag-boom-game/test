// Inherit the parent event
event_inherited();
wait = 0; 
timePassed = 0
waitDuration = random_range(40,100);
xTo = xstart
yTo = ystart
enemyHP = 100000000000;

//Enemy Sprites
sprMove = sButterfly;
sprIdle = sButterfly;

//Enemy Scripts
enemyScript[ENEMYSTATE.WANDER] = ButterflyWander
enemyScript[ENEMYSTATE.WAIT] = EnemyWait

state = ENEMYSTATE.WANDER

original_scale = 0.3;

image_xscale *= original_scale;
image_yscale *= original_scale;
