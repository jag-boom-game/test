if (room = rCave1) {
	if (surface_exists(light_surf)) {
	
	surface_set_target(light_surf);
	draw_clear(c_black);
	
	with (oBlueShield) {
		gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_LightSmall, 0, x, y-sprite_height/2.5, 0.25, 0.25,0, c_white, Wave(0.1,0.3,8,0));
	}
	with (oBroadSword) {
		gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_LightSmall, 0, x, y-sprite_height/2.5, 0.25, 0.25,0, c_white, Wave(0.1,0.3,8,0));
	}
	
	with (oPlayer) {
		gpu_set_blendmode(bm_subtract);
		draw_sprite_ext(spr_LightSmall, 0, x, y-sprite_height/2.5, 0.5, 0.5,0, c_white, 0.7);
		
		gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_LightSmall, 0, x, y-sprite_height/2.5, 0.5, 0.5,0, c_white, Wave(0.5,0.7,2,0));
	}
	
	with (oGoldCoin) {
		gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_LightSmall, 0, x, y-sprite_height/2.5, 0.5, 0.5,0, c_white, Wave(0.5,0.7,2,0));
	}
	
	with (oGoldCoins) {
		gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_LightSmall, 0, x, y-sprite_height/2.5, 0.5, 0.5,0, c_white, Wave(0.5,0.7,2,0));
	}
	
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
	draw_surface_ext(light_surf,0,0,1,1,0,c_white,darkness);
	
} else {
		light_surf = surface_create(room_width,room_height);
	}
}