// Get Player Input
#region //Input
if (instance_exists(oTitle)) || (instance_exists(oKeyBindings)) {
global.keyleft = keyboard_check_pressed(ds_map_find_value(keyBindings,0));
global.keyright = keyboard_check_pressed(ds_map_find_value(keyBindings,1));
global.keyup = keyboard_check_pressed(ds_map_find_value(keyBindings,2));
global.keydown = keyboard_check_pressed(ds_map_find_value(keyBindings,3));
} else {
global.keyleft = keyboard_check(ds_map_find_value(keyBindings,0));
global.keyright = keyboard_check(ds_map_find_value(keyBindings,1));
global.keyup = keyboard_check(ds_map_find_value(keyBindings,2));
global.keydown = keyboard_check(ds_map_find_value(keyBindings,3));
}
global.keyActivate = keyboard_check_pressed(ds_map_find_value(keyBindings,4));
global.keyAttack = keyboard_check_pressed(ds_map_find_value(keyBindings,5));
global.keyMagic = keyboard_check_pressed(ds_map_find_value(keyBindings,6));
global.keyItem = keyboard_check_pressed(ds_map_find_value(keyBindings,7));
global.keyMenu = keyboard_check_pressed(ds_map_find_value(keyBindings,8));
global.keyCommitted = keyboard_check_pressed(ds_map_find_value(keyBindings,9));

#endregion

if (keyboard_check_pressed(ord("F"))) {
	if (window_get_fullscreen() = false) {window_set_fullscreen(true)} else {window_set_fullscreen(false)}
}

if (room != rTitle) {
	if (!instance_exists(oPlayer)) {instance_create_depth(424,55,depth,oPlayer)}
}

if (!instance_exists(oCamera)) 
	if (instance_exists(oPlayer)) {
		{global.iCamera = instance_create_depth(oPlayer.x,oPlayer.y,depth,oCamera);}
	}
	
if (room = rTitle) {
	global.hp = global.maxhp;
}

global.levelText = string("Level: " + string(global.level));
global.xpText = string("EXP: " + string(global.xp) + "/" + string(global.maxxp));
global.attText = string("ATT: " + string(global.ATT));

if (keyboard_check_pressed(vk_escape)) {game_end();}