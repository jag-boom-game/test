// Globals & Initialise
//randomize();

global.gamePaused = false;
global.textSpeed = 0.75;
global.targetRoom = -1;
global.targetX = -1;
global.targetY = -1;
global.targetDirection = 0;
global.iLifted = noone;
global.hp = 100;
global.maxhp = 100;
global.mana = 20;
global.maxmana = 20;

global.stamina = 300;
global.maxstamina = global.stamina;
global.level = 1;
global.xp = 0;
global.maxxp = 3;
global.ATT = 1;
global.playerName = choose("Geoffrey","Reggie","Aaron","Jessie","Agamemnon");

if (instance_exists(oPlayer)) {
global.iCamera = instance_create_layer(oPlayer.x,oPlayer.y,layer,oCamera);
}

journal_entries_locked = ds_map_create();

for (var i = 0; i <= 30; i++) {
	ds_map_add(journal_entries_locked,i,false);
}

if (room == rInit) {
display_set_gui_size(RESOLUTION_W,RESOLUTION_H)
room_goto(ROOM_START);
} 

left = "A";
right = "D";
up = "W";
down = "S";
roll = "K";
slash = "L";
magic = "J";
item = "I";
back = "";

keys = ds_list_create();
ds_list_add(keys,left,right,up,down,roll,slash,magic,item,back);

keyBindings = ds_map_create();
ds_map_add(keyBindings,0,ord(left)); //Left Move Key
ds_map_add(keyBindings,1,ord(right)); //Right Move Key
ds_map_add(keyBindings,2,ord(up)); //Up Move Key
ds_map_add(keyBindings,3,ord(down)); //Down Move Key
ds_map_add(keyBindings,4,ord(roll)); //Roll Key
ds_map_add(keyBindings,5,ord(slash)); //Slash Key
ds_map_add(keyBindings,6,ord(magic)); //Magic Key
ds_map_add(keyBindings,7,ord(item)); //Item Key
ds_map_add(keyBindings,8,vk_backspace); //Pause Menu Key
ds_map_add(keyBindings,9,vk_enter); //Confirm Key