//Initializing Player Variables
state = PlayerStateFree;
stateAttack = AttackSlash;
hitByAttack = -1;
lastState = state;

collisionmap = layer_tilemap_get_id(layer_get_id("Col"));

image_speed = 0;
hspd = 0;
vspd = 0;
speedwalk = 2.0;
speedRoll = 3.0;
distanceRoll = 52;
distanceBonk = 40;
distanceBonkHeight = 12;
speedBonk = 1.5;
z = 0;
new_journal = false;

//chest = sChainMailm;
//boots = sBoots;
//helmet = sCap;
//gloves = sGloves;

animationEndScript = -1;

spriteRoll = sPlayerRoll;
spriteRun = sPlayerRun2;
spriteIdle = sPlayer2;
localFrame = 0;
magic = Fire;

invincible = false;

if (global.targetX != -1)
{
	x = global.targetX;
	y = global.targetY;
	direction = global.targetDirection;
}
