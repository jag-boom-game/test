if (global.hp >= 1) {
	draw_sprite(sShadow,0,floor(x),floor(y));

	draw_sprite_ext(
		sprite_index,
		image_index,
		floor(x),
		floor(y-z),
		image_xscale,
		image_yscale,
		image_angle,
		image_blend,
		image_alpha
	)

	if (invincible = true) {
		image_alpha = Wave(0,0.5,0.5,0);
	
		//Draw Health Bar
		
		if (global.hp <= global.maxhp) && (global.hp >= global.maxhp*2/3) {
			draw_set_color(c_lime);
		}
		
		if (global.hp < global.maxhp*2/3) && (global.hp >= global.maxhp/3) {
			draw_set_color(c_yellow);
		}
		
		if (global.hp < global.maxhp/3) && (global.hp > 0) {
			draw_set_color(c_red);
		} 
		
		draw_rectangle(x-sprite_get_width(spriteRun),
		y-sprite_get_height(spriteRun),
		(x-sprite_get_width(spriteRun))+(((x+sprite_get_width(spriteRun)-(x-sprite_get_width(spriteRun)))*global.hp)/global.maxhp),
		y-sprite_get_height(spriteRun)-sprite_get_width(spriteRun)/4,
		false);
	
		//Health Bar Outline
		draw_set_color(c_white);
		draw_rectangle(x-sprite_get_width(spriteRun),
		y-sprite_get_height(spriteRun),
		x+sprite_get_width(spriteRun),
		y-sprite_get_height(spriteRun)-sprite_get_width(spriteRun)/4,
		true);
	
		//Draw Mana Bar
		draw_set_color(c_blue)
		draw_rectangle(x-sprite_get_width(spriteRun),
		y-sprite_get_height(spriteRun),
		(x-sprite_get_width(spriteRun))+(((x+sprite_get_width(spriteRun)-(x-sprite_get_width(spriteRun)))*global.mana)/global.maxmana),
		y-sprite_get_height(spriteRun)+sprite_get_width(spriteRun)/8,
		false);
	
		//Mana Bar Outline
		draw_set_color(c_white);
		draw_rectangle(x-sprite_get_width(spriteRun),
		y-sprite_get_height(spriteRun),
		x+sprite_get_width(spriteRun),
		y-sprite_get_height(spriteRun)+sprite_get_width(spriteRun)/8,
		true);
	}
}

//Level Up Text
if (instance_exists(oLevelUp)) {
	draw_text_ext_transformed_color(x-string_width("Level Up!")/8,y-Wave(0,2,1,0),"Level Up!",0,128,0.25,0.25,0,c_black,c_black,c_black,c_black,1);
	draw_text_ext_transformed_color(x-string_width("Level Up!")/8,y-Wave(0,2,1,0),"Level Up!",0,128,0.24,0.24,0,c_white,c_white,c_white,c_white,1);
}
//Draw Equipment

/*
//Chest Equipment
if (sprite_index == spriteRun) {
	draw_sprite(chest,image_index,x,y-1);
} else {draw_sprite(chest,CARDINAL_DIR*9,x,y-1)}

//Boots
if (sprite_index == spriteRun) {
	draw_sprite(boots,image_index,x-1,y);
} else {draw_sprite(boots,CARDINAL_DIR*9,x-1,y)}

//Helmet
if (sprite_index == spriteRun) {
	draw_sprite(helmet,image_index,x-1,y);
} else {draw_sprite(helmet,CARDINAL_DIR*9,x-1,y)}

//Helmet
if (sprite_index == spriteRun) {
	draw_sprite(gloves,image_index,x,y);
} else {draw_sprite(gloves,CARDINAL_DIR*9,x,y)}
*/