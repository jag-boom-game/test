
//Directional and Movement Calculations
inputdirection = point_direction(0,0,global.keyright-global.keyleft,global.keydown-global.keyup);
inputmagnitude = (global.keyright - global.keyleft!=0) or (global.keyup - global.keydown!=0);

//Player Starts in Free State
if (oPlayer.state == undefined) {oPlayer.state = PlayerStateFree;}

//Creates Pause Functionality
if (!global.gamePaused) script_execute (state);

//Creates Virtual Depth
depth = -bbox_bottom;

//Save Game Function
if (place_meeting(x,y,oSavePoint)) && (keyboard_check_pressed(vk_control)) {
	
	if (!instance_exists(oSaveMenu)) {	
		global.gamePaused = true;
		instance_create_depth(0,0,depth,oSaveMenu);
	} else {}
}

//Save Point Functions (Healing and Create Interact Triangle)
if (place_meeting(x,y,oSavePoint)) {
	if (global.hp != global.maxhp) {
		global.hp++;	
	}
	if (global.mana != global.maxmana) {
		global.mana++;	
	}
	if (!instance_exists(oTriangle)) {
		instance_create_depth(x,y,depth,oTriangle);
	}
} else {
	if (instance_exists(oTriangle)) {
		instance_destroy(oTriangle);
	}
}


if (!instance_exists(oGame)) {instance_create_depth(x,y,depth,oGame);}
if (!instance_exists(oLighting)) {instance_create_depth(x,y,depth,oLighting);}

//Game Over
if (global.hp <= 0) {
	global.gamePaused = true;
	if (!instance_exists(oGameOver)) {
		instance_create_depth(x,y,depth,oGameOver)
	}
}

if (global.keyMenu) {
	if (!instance_exists(oPauseMenu)) {
		instance_create_depth(x,y,depth,oPauseMenu);
		global.gamePaused = true; 
	}
}

//Diagnostic Equipment Toggle
if (keyboard_check_pressed(vk_numpad1)) {
	if (helmet != sGoldHelmet) {helmet = sGoldHelmet;} else {helmet = sCap;}
}
if (keyboard_check_pressed(vk_numpad2)) {
	if (boots != sGoldBoots) {boots = sGoldBoots;} else {boots = sBoots;}
}
if (keyboard_check_pressed(vk_numpad3)) {
	if (chest != sGoldArmor) {chest = sGoldArmor;} else {chest = sChainMailm;}
}