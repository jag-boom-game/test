if (global.gamePaused = false) && (!instance_exists(oText)) {
#region	//Stats Display GUI
	draw_set_halign(fa_left);
	draw_set_alpha(0.5)

	//Stats Box
	draw_rectangle_color(display_get_gui_width()/2,display_get_gui_height()-display_get_gui_height()*0.1,display_get_gui_width(),display_get_gui_height(),c_blue,c_navy,c_blue,c_navy,false);
	draw_set_alpha(1);
	draw_set_color(c_white);
	draw_set_font(fnt_GUI);
#endregion

#region	//Name Display
	draw_set_color(c_black);
	draw_text_transformed(display_get_gui_width()*3/6 + string_width(global.playerName)/25+0.5, display_get_gui_height()-display_get_gui_height()*0.1+string_height(global.playerName)/2*.25+0.5,global.playerName,0.25,0.25,0);
	draw_set_color(c_white);
	draw_text_transformed(display_get_gui_width()*3/6 + string_width(global.playerName)/25, display_get_gui_height()-display_get_gui_height()*0.1+string_height(global.playerName)/2*.25,global.playerName,0.25,0.25,0);
#endregion

#region	//Level Display
	draw_set_color(c_black);
	draw_text_transformed(display_get_gui_width()*3/6 + string_width(global.levelText)/25+0.5, display_get_gui_height()-display_get_gui_height()*0.05+string_height(global.levelText)/2*.25+0.5,global.levelText,0.25,0.25,0);
	draw_set_color(c_white);
	draw_text_transformed(display_get_gui_width()*3/6 + string_width(global.levelText)/25, display_get_gui_height()-display_get_gui_height()*0.05+string_height(global.levelText)/2*.25,global.levelText,0.25,0.25,0);
#endregion

#region	//HP Display
	draw_set_color(c_black)
	draw_text_transformed(display_get_gui_width()*4/6 + string_width("HP: ")/2*.25+0.5, display_get_gui_height()-display_get_gui_height()*0.1+string_height("HP: ")/2*.25+0.5,"HP: " + string(global.hp) + "/" + string(global.maxhp),0.25,0.25,0);
	if (global.hp <= global.maxhp) && (global.hp >= global.maxhp*2/3) {
		draw_set_color(c_white);
	}
		
	if (global.hp < global.maxhp*2/3) && (global.hp >= global.maxhp/3) {
		draw_set_color(c_yellow);
	}
		
	if (global.hp < global.maxhp/3) && (global.hp > 0) {
		draw_set_color(c_red);
	} 
	draw_text_transformed(display_get_gui_width()*4/6 + string_width("HP: ")/2*.25, display_get_gui_height()-display_get_gui_height()*0.1+string_height("HP: ")/2*.25,"HP: " + string(global.hp) + "/" + string(global.maxhp),0.25,0.25,0);

	draw_set_color(c_white);
#endregion

#region	//EXP Display
	draw_set_color(c_black)
	draw_text_transformed(display_get_gui_width()*4/6 + string_width(global.xpText)/2*.25+0.5, display_get_gui_height()-display_get_gui_height()*0.05+string_height(global.xpText)/2*.25+0.5,global.xpText,0.25,0.25,0);
	draw_set_color(c_white);
	draw_text_transformed(display_get_gui_width()*4/6 + string_width(global.xpText)/2*.25, display_get_gui_height()-display_get_gui_height()*0.05+string_height(global.xpText)/2*.25,global.xpText,0.25,0.25,0);

#endregion

#region	//Mana Display
	draw_set_color(c_black);
	draw_text_transformed(display_get_gui_width()*5/6 + string_width("Mana: ")/2*.25+0.5, display_get_gui_height()-display_get_gui_height()*0.1+string_height("Mana: ")/2*.25+0.5,"Mana: " + string(global.mana) + "/" + string(global.maxmana),0.25,0.25,0);
	draw_set_color(c_white);
	draw_text_transformed(display_get_gui_width()*5/6 + string_width("Mana: ")/2*.25, display_get_gui_height()-display_get_gui_height()*0.1+string_height("Mana: ")/2*.25,"Mana: " + string(global.mana) + "/" + string(global.maxmana),0.25,0.25,0);
#endregion

#region	//ATT Display
	draw_set_color(c_black);
	draw_text_transformed(display_get_gui_width()*5/6 + string_width(global.attText)/2*.25+0.5, display_get_gui_height()-display_get_gui_height()*0.05+string_height(global.attText)/2*.25+0.5,global.attText,0.25,0.25,0);
	draw_set_color(c_white);
	draw_text_transformed(display_get_gui_width()*5/6 + string_width(global.attText)/2*.25, display_get_gui_height()-display_get_gui_height()*0.05+string_height(global.attText)/2*.25,global.attText,0.25,0.25,0);
#endregion



}

if (new_journal == true) {
	draw_set_halign(fa_center);
	draw_set_font(fnt_Default);
	draw_text_transformed(15,5,"New Journal Entry Added!",0.25,0.25,0);
}

	//Diagnostics
	//draw_text(10,10,string(CARDINAL_DIR))
	/*
	draw_line(display_get_gui_width()/2,0,display_get_gui_width()/2,room_height);
	draw_line(0,display_get_gui_height()/2,room_width,display_get_gui_height()/2);