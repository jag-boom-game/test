/// Destroy XP orbs
with (other) instance_destroy();

//Add XP and Level Up
	global.xp += 1;
	if (global.xp >= global.maxxp) {
		instance_create_depth(x,y,depth-1,oLevelUp);
		global.level += 1;
		global.xp = global.xp - global.maxxp;
		global.maxxp = round(global.maxxp * 1.2);
		global.maxhp = round (global.maxhp * 1.5);
		global.hp = global.maxhp;
		global.maxmana = round (global.maxmana * 1.5);
		global.mana = global.maxmana;
		global.maxstamina = round(global.maxstamina * 1.5);
		global.stamina = global.maxstamina;
		global.ATT++;
		}