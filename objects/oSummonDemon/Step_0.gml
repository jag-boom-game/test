if (!instance_exists(creator)) {instance_destroy();}

if (!global.gamePaused) {
	image_speed = 0.25;
} else {image_speed = 0;}

depth = -bbox_bottom;

if (image_index == round(image_number/2)) {instance_create_depth(x,y-4,depth,oButterfly);}

if (enemyHP <= 0) {instance_destroy();}